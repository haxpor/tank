*Note: I developed this project back in November 2008, and I decided to dig it again and make it available for anyone who would love to see it (not sure how many ;))

Tank was an experimental project for model's structure manipulation and transformation, and home-made physics for Tank's movement on the ground.

It applied the rendering with simple shadow map (but currently messed up and I don't have time to fix it now). The terrain was generated with heightmap. Skydome was made to life with simplistic approach to transition the cloud map over its mesh.

You can do whatever you want with the source code. Hope you like it.
Yes, just go to https://bitbucket.org/haxpor/tank or directly fork my project at git@bitbucket.org:haxpor/tank.git.

-- Technical note --
It's created with Visual Studio Express 2008 with XNA 3.0. That should be enough for Windows-based development system.

Wasin