﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace TankMovementAnimation
{
    class Terrain
    {
        public struct MultitexturedVertex
        {
            public Vector3 Position;
            public Vector3 Normal;
            public Vector2 TextureCoords;
            public Vector4 TextureWeights;

            public static int SizeInBytes = sizeof(float) * (3 + 3 + 2 + 4);
            public static readonly VertexElement[] VertexElements = 
            {
                new VertexElement(0, 0, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Position, 0),
                new VertexElement(0, sizeof(float) * 3, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Normal, 0),
                new VertexElement(0, sizeof(float) * (3+3), VertexElementFormat.Vector2, VertexElementMethod.Default, VertexElementUsage.TextureCoordinate, 0),
                new VertexElement(0, sizeof(float) * (3+3+2), VertexElementFormat.Vector4, VertexElementMethod.Default, VertexElementUsage.TextureCoordinate, 1)
            };
        };

        GraphicsDevice device;
        Effect effect;

        Texture2D heightMap;

        MultitexturedVertex[] vertices;
        short[] indices;
        float[,] terrainHeightData;

        //Actual non-scaled terrain width
        int terrainWidth;
        //Actual non-scaled terrain height
        int terrainHeight;

        float minHeight;
        float maxHeight;
        float scaledWidth = 1.0f;   //default
        float scaledHeight = 1.0f;  //default

        float TexCoordScaledHeight = 30.0f;

        //Scaled terrain width
        float terrainScaledWidth;
        //Scaled terrain height
        float terrainScaledHeight;

        Texture2D sandTexture;
        Texture2D grassTexture;
        Texture2D rockTexture;
        Texture2D snowTexture;

        VertexBuffer vertexBuffer;
        IndexBuffer indexBuffer;
        VertexDeclaration vertexDeclaration;

        Matrix worldAdjust;

        //Misc Settings
        public LightSettings LightSettings;
        public EffectSettings EffectSettings;

        //Set and Get
        public float TerrainScaledWidth
        {
            get { return terrainScaledWidth; }
        }

        public float TerrainScaledHeight
        {
            get { return terrainScaledHeight; }
        }

        // Constructor
        public Terrain(string assetName, ContentManager Content, GraphicsDevice device, ref Effect effect)
        {
            heightMap = Content.Load<Texture2D>(assetName);
            this.effect = effect;
            terrainWidth = heightMap.Width;
            terrainHeight = heightMap.Height;
            this.device = device;

            terrainScaledWidth = terrainWidth * scaledWidth;
            terrainScaledHeight = terrainHeight * scaledHeight;

            sandTexture = Content.Load<Texture2D>("sand");
            grassTexture = Content.Load<Texture2D>("grass");
            rockTexture = Content.Load<Texture2D>("rock");
            snowTexture = Content.Load<Texture2D>("snow");

            worldAdjust = Matrix.CreateTranslation(-terrainScaledWidth / 2.0f, 0.0f, -terrainScaledHeight / 2.0f);

            LightSettings = new LightSettings();
            EffectSettings = new EffectSettings();
        }

        // Load and prepare the space in array after we know the width and height of terrain
        public void Load()
        {
            vertices = new MultitexturedVertex[terrainWidth * terrainHeight];
            indices = new short[(terrainWidth-1) * (terrainHeight-1) * 6];
            terrainHeightData = new float[terrainWidth , terrainHeight];

            //load the height data from height map
            SetHeightData();
            //set up the vertices only in x and z axis
            SetUpVertices();
            //set up the vertices indices
            SetUpIndices();
            //Calculate the normals
            CalculateNormals();
            //Copy to buffers
            CopyToBuffers();
        }

        // Set up the height data before setting the full vertices setting
        private void SetHeightData()
        {
            Color[] colorData = new Color[terrainWidth * terrainHeight];
            heightMap.GetData<Color>(colorData);

            minHeight = float.MaxValue;
            maxHeight = float.MinValue;

            for (int x = 0; x < terrainWidth; x++)
            {
                for (int y = 0; y < terrainHeight; y++)
                {
                    int index = x + y*terrainWidth;
                    terrainHeightData[x,y] = colorData[index].R;
                    if (terrainHeightData[x, y] < minHeight) minHeight = terrainHeightData[x, y];
                    else if (terrainHeightData[x, y] > maxHeight) maxHeight = terrainHeightData[x, y];
                }
            }

            //bound the terrain height
            float diffHeight = maxHeight - minHeight;
            for (int x = 0; x < terrainWidth; x++)
            {
                for (int y = 0; y < terrainHeight; y++)
                {
                    terrainHeightData[x, y] = (terrainHeightData[x, y] - minHeight) / (diffHeight) * TexCoordScaledHeight;
                }
            }
        }

        // Set up the terrain's vertices
        private void SetUpVertices()
        {
            for (int x = 0; x < terrainWidth; x++)
            {
                for (int z = 0; z < terrainHeight; z++)
                {
                    int index = x + z * terrainWidth;

                    vertices[index].Position = new Vector3(x * scaledWidth, terrainHeightData[x,z] * scaledHeight, z * scaledHeight);

                    //map the texture
                    vertices[index].TextureCoords.X = (float)x / TexCoordScaledHeight;
                    vertices[index].TextureCoords.Y = (float)z / TexCoordScaledHeight;

                    //mapping scheme (must be change if TexCoordScaledHeight is modified)
                    vertices[index].TextureWeights.X = MathHelper.Clamp(1.0f - Math.Abs(terrainHeightData[x, z] - 0.0f) / 8.0f, 0.0f, 1.0f);
                    vertices[index].TextureWeights.Y = MathHelper.Clamp(1.0f - Math.Abs(terrainHeightData[x, z] - 12.0f) / 6.0f, 0.0f, 1.0f);
                    vertices[index].TextureWeights.Z = MathHelper.Clamp(1.0f - Math.Abs(terrainHeightData[x, z] - 20.0f) / 6.0f, 0.0f, 1.0f);
                    vertices[index].TextureWeights.W = MathHelper.Clamp(1.0f - Math.Abs(terrainHeightData[x, z] - 30.0f) / 6.0f, 0.0f, 1.0f);

                    //average all of them
                    float total = vertices[index].TextureWeights.X;
                    total += vertices[index].TextureWeights.Y;
                    total += vertices[index].TextureWeights.Z;
                    total += vertices[index].TextureWeights.W;

                    vertices[index].TextureWeights.X /= total;
                    vertices[index].TextureWeights.Y /= total;
                    vertices[index].TextureWeights.Z /= total;
                    vertices[index].TextureWeights.W /= total;
                }
            }

            vertexDeclaration = new VertexDeclaration(device, MultitexturedVertex.VertexElements);
        }

        // Set up the indices
        private void SetUpIndices()
        {
            int count = 0;

            for (int y = 0; y < terrainHeight - 1; y++)
            {
                for (int x = 0; x < terrainWidth - 1; x++)
                {
                    int topLeft = (x + (y * terrainWidth));
                    int topRight = ((x + 1) + (y * terrainWidth));
                    int lowerLeft = (x + ((y + 1) * terrainWidth));
                    int lowerRight = ((x + 1) + ((y + 1) * terrainWidth));

                    indices[count++] = (short)topLeft;
                    indices[count++] = (short)lowerRight;
                    indices[count++] = (short)lowerLeft;
                    indices[count++] = (short)topLeft;
                    indices[count++] = (short)topRight;
                    indices[count++] = (short)lowerRight;
                }
            }
        }

        // Calculate normals for terrain
        private void CalculateNormals()
        {
            //clear out the normals for all vertices first
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i].Normal = new Vector3(0, 0, 0);
            }

            //calculate the face normal
            for (int i = 0; i < indices.Length / 3; i++)
            {
                short index1 = indices[i * 3];
                short index2 = indices[i * 3 + 1];
                short index3 = indices[i * 3 + 2];

                Vector3 origin = vertices[index1].Position;
                Vector3 dr = vertices[index2].Position;
                Vector3 dl = vertices[index3].Position;
                dr -= origin;
                dl -= origin;
                Vector3 normal = Vector3.Cross(dl, dr);

                vertices[index1].Normal += normal;
                vertices[index2].Normal += normal;
                vertices[index3].Normal += normal;
            }

            //normalize all the normals
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i].Normal.Normalize();
            }
        }

        // Copy to buffers
        private void CopyToBuffers()
        {
            //vertex buffer
            vertexBuffer = new VertexBuffer(device, vertices.Length * MultitexturedVertex.SizeInBytes, BufferUsage.WriteOnly);
            vertexBuffer.SetData<MultitexturedVertex>(vertices);

            //index buffer
            indexBuffer = new IndexBuffer(device, typeof(short), indices.Length, BufferUsage.WriteOnly);
            indexBuffer.SetData<short>(indices);
        }

        // Get the height of terrain from the specified x and z coordinate
        public float GetHeight(Vector3 position)
        {
            Vector3 positionOnHeightMap = position - worldAdjust.Translation;

            //get the index used in heightmap array
            int left, top;
            left = (int)(positionOnHeightMap.X / scaledWidth);
            top = (int)(positionOnHeightMap.Z / scaledHeight);

            //calculate the amount of x and z
            float xNormalized = (positionOnHeightMap.X % scaledWidth) / scaledWidth;
            float zNormalized = (positionOnHeightMap.Z % scaledHeight) / scaledHeight;

            //use bilinear interpolation to calculate the height
            float topHeight = MathHelper.Lerp(terrainHeightData[left, top], terrainHeightData[left + 1, top], xNormalized);
            float bottomHeight = MathHelper.Lerp(terrainHeightData[left, top + 1], terrainHeightData[left + 1, top + 1], zNormalized);

            return MathHelper.Lerp(topHeight, bottomHeight, zNormalized);
        }

        // General purpose draw method
        public void Draw(string technique, GameTime gameTime, Matrix world, Matrix view, Matrix projection)
        {
            // TODO: Add your drawing code here
            effect.CurrentTechnique = effect.Techniques[technique];
            effect.Parameters["xView"].SetValue(view);
            effect.Parameters["xProjection"].SetValue(projection);
            effect.Parameters["xLightView"].SetValue(LightSettings.LightView);
            effect.Parameters["xLightProjection"].SetValue(LightSettings.LightProjection);
            effect.Parameters["xWorld"].SetValue(worldAdjust);
            effect.Parameters["xLightEnabled"].SetValue(LightSettings.LightEnabled);
            effect.Parameters["xLightDirection"].SetValue(LightSettings.LightLookAt - LightSettings.LightPosition);
            effect.Parameters["xLightPosition"].SetValue(LightSettings.LightPosition);
            effect.Parameters["xAmbient"].SetValue(LightSettings.LightAmbient);
            effect.Parameters["xLightPower"].SetValue(LightSettings.LightPower);
            effect.Parameters["xTexture0"].SetValue(grassTexture);
            effect.Parameters["xTexture1"].SetValue(rockTexture);
            effect.Parameters["xTexture2"].SetValue(snowTexture);
            effect.Parameters["xTexture3"].SetValue(snowTexture);
            effect.Parameters["xLightType"].SetValue(LightSettings.LightType);

            if (EffectSettings.eShadowedScene.Enabled)
            {
                effect.Parameters["xBiasedDistance"].SetValue(EffectSettings.eShadowedScene.BiasedDistance);
                effect.Parameters["xShadowMap"].SetValue(EffectSettings.eShadowedScene.ShadowMap);
            }
            if (EffectSettings.eShadowedScene.BaseCustomDiffuseColorEnabled)
            {
                effect.Parameters["xBaseDiffuseColorFactor"].SetValue(EffectSettings.eShadowedScene.BaseDiffuseColorFactor);
            }

            effect.Begin();

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();

                device.VertexDeclaration = vertexDeclaration;
                //device.RenderState.FillMode = FillMode.WireFrame;
                device.Indices = indexBuffer;
                device.Vertices[0].SetSource(vertexBuffer, 0, MultitexturedVertex.SizeInBytes);
                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertexBuffer.SizeInBytes / MultitexturedVertex.SizeInBytes,
                    0, indexBuffer.SizeInBytes / sizeof(short) / 3);

                //device.RenderState.FillMode = FillMode.Solid;

                pass.End();
            }
            effect.End();
        }
    }
}
