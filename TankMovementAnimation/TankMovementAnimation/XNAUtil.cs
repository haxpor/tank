﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TankMovementAnimation
{
    class XNAUtil
    {
        //contain the light properties for the XNAUtil
        public static LightSettings LightSettings = new LightSettings();

        //#1 Without bounding sphere
        //#1.1 No preloaded bounding sphere
        // Load model
        public static void LoadModel(ContentManager Content, string assetName, out Model model)
        {
            //Load model from assetName
            model = Content.Load<Model>(assetName);
        }

        // Load model with basic effect
        public static void LoadModel(GraphicsDevice device, ContentManager Content, string assetName, ref BasicEffect basicEffect, out Model model)
        {
            //Load model from assetName
            model = Content.Load<Model>(assetName);

            //loop and set the basic effect
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = basicEffect.Clone(device);
                }
            }
        }

        // Load model with basic effect, textures
        public static void LoadModel(GraphicsDevice device, ContentManager Content, string assetName, ref BasicEffect basicEffect, out Texture2D[] textures, out Model model)
        {
            //Load model from assetName
            model = Content.Load<Model>(assetName);
            //allocate the array for textures of model
            textures = new Texture2D[model.Meshes.Count];

            //index-running for the textures of array
            int i = 0;

            //load the model's accessories
            foreach (ModelMesh mesh in model.Meshes)
            {
                //load textures
                foreach (BasicEffect local_effect in mesh.Effects)
                {
                    textures[i++] = local_effect.Texture;
                }

                //load effect
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = basicEffect.Clone(device);
                }
            }
        }

        // Load model with the specified effect
        public static void LoadModel(GraphicsDevice device, ContentManager Content, string assetName, ref Effect effect, out Model model)
        {
            //Load model from assetName
            model = Content.Load<Model>(assetName);

            //loop and set the effect
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = effect.Clone(device);
                }
            }
        }

        // Load model that attached with textures
        public static void LoadModel(GraphicsDevice device, ContentManager Content, string assetName, ref Effect effect, out Texture2D[] textures, out Model model)
        {
            //Load model from assetName
            model = Content.Load<Model>(assetName);
            //allocate the array for textures of model
            textures = new Texture2D[model.Meshes.Count];

            //index-running for the textures of array
            int i = 0;

            //load the model's accessories
            foreach (ModelMesh mesh in model.Meshes)
            {
                //load textures
                foreach (BasicEffect local_effect in mesh.Effects)
                {
                    textures[i++] = local_effect.Texture;
                }

                //load effect
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = effect.Clone(device);
                }
            }
        }

        //#1.2 Preloaded model
        //Load model with the basic effect
        public static void LoadModel(GraphicsDevice device, ContentManager Content, ref Model model, ref BasicEffect basicEffect)
        {
            //loop and set the basic effect
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach(ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = basicEffect.Clone(device);
                }
            }
        }

        //Load model with the basic effect, textures
        public static void LoadModel(GraphicsDevice device, ContentManager Content, ref Model model, ref BasicEffect basicEffect, out Texture2D[] textures)
        {
            //allocate the array for textures of model
            textures = new Texture2D[model.Meshes.Count];

            //index-running for the textures of array
            int i = 0;

            //load the model's accessories
            foreach (ModelMesh mesh in model.Meshes)
            {
                //load textures
                foreach (BasicEffect local_effect in mesh.Effects)
                {
                    textures[i++] = local_effect.Texture;
                }

                //load effect
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = basicEffect.Clone(device);
                }
            }
        }

        // Load model with the specified effect
        public static void LoadModel(GraphicsDevice device, ContentManager Content, ref Model model, ref Effect effect)
        {
            //loop and set the effect
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = effect.Clone(device);
                }
            }
        }

        // Load model that attached with textures
        public static void LoadModel(GraphicsDevice device, ContentManager Content, ref Model model, ref Effect effect, out Texture2D[] textures)
        {
            //allocate the array for textures of model
            textures = new Texture2D[model.Meshes.Count];

            //index-running for the textures of array
            int i = 0;

            //load the model's accessories
            foreach (ModelMesh mesh in model.Meshes)
            {
                //load textures
                foreach (BasicEffect local_effect in mesh.Effects)
                {
                    textures[i++] = local_effect.Texture;
                }

                //load effect
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = effect.Clone(device);
                }
            }
        }

        //#2 With bounding sphere
        //#2.1 No preloaded model
        //Load model with bounding sphere and with the basic effect
        public static void LoadModelWithBoundingSphere(GraphicsDevice device, ContentManager Content, string assetName, ref BasicEffect basicEffect, out Model model)
        {
            //Load model
            LoadModel(device, Content, assetName, ref basicEffect, out model);

            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            model.Tag = completedBoundingSphere;
        }

        //Load model with bounding sphere and with the basic effect, textures
        public static void LoadModelWithBoundingSphere(GraphicsDevice device, ContentManager Content, string assetName, ref BasicEffect basicEffect, out Texture2D[] textures, out Model model)
        {
            //Load model
            LoadModel(device, Content, assetName, ref basicEffect, out textures, out model);

            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            model.Tag = completedBoundingSphere;
        }

        // Load model with bounding sphere but without effect and textures
        public static void LoadModelWithBoundingSphere(GraphicsDevice device, ContentManager Content, string assetName, out Model model)
        {
            //Load model
            LoadModel(Content, assetName, out model);

            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            model.Tag = completedBoundingSphere;
        }

        // Load model with bounding sphere but without textures
        public static void LoadModelWithBoundingSphere(GraphicsDevice device, ContentManager Content, string assetName, ref Effect effect, out Model model)
        {
            //Load model
            LoadModel(device, Content, assetName, ref effect, out model);

            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            model.Tag = completedBoundingSphere;
        }

        //Load model with bounding sphere
        public static void LoadModelWithBoundingSphere(GraphicsDevice device, ContentManager Content, string assetName, ref Effect effect, out Texture2D[] textures, out Model model)
        {
            //Load model
            LoadModel(device, Content, assetName, ref effect, out textures, out model);

            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            model.Tag = completedBoundingSphere;
        }

        //#2.2 Preloaded model
        // Load model with bounding sphere but without effect and textures (preload model)
        public static void LoadModelWithBoundingSphere(GraphicsDevice device, ContentManager Content, ref Model model)
        {
            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            model.Tag = completedBoundingSphere;
        }

        // Load model with bounding sphere but without textures (preloaded model)
        //with basic effect
        public static void LoadModelWithBoundingSphere(GraphicsDevice device, ContentManager Content, ref Model model, ref BasicEffect basicEffect)
        {
            //loop and set the effect
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = basicEffect.Clone(device);
                }
            }

            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            model.Tag = completedBoundingSphere;
        }

        // Load model with bounding sphere (preloaded model) with textures
        //with basic effect
        public static void LoadModelWithBoundingSphere(GraphicsDevice device, ContentManager Content, ref Model model, ref BasicEffect basicEffect, out Texture2D[] textures)
        {
            //allocate the space for texture array
            textures = new Texture2D[model.Meshes.Count];

            //used as the textures running loop
            int i = 0;

            //load the model's accessories
            foreach (ModelMesh mesh in model.Meshes)
            {
                //load textures
                foreach (BasicEffect local_effect in mesh.Effects)
                {
                    textures[i++] = local_effect.Texture;
                }

                //load effect
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = basicEffect.Clone(device);
                }
            }

            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            model.Tag = completedBoundingSphere;
        }

        // Load model with bounding sphere but without textures (preloaded model)
        public static void LoadModelWithBoundingSphere(GraphicsDevice device, ContentManager Content, ref Model model, ref Effect effect)
        {
            //loop and set the effect
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = effect.Clone(device);
                }
            }

            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            model.Tag = completedBoundingSphere;
        }

        // Load model with bounding sphere (preloaded model)
        public static void LoadModelWithBoundingSphere(GraphicsDevice device, ContentManager Content, ref Model model, ref Effect effect, out Texture2D[] textures)
        {
            //allocate the space for texture array
            textures = new Texture2D[model.Meshes.Count];

            //used as the textures running loop
            int i = 0;

            //load the model's accessories
            foreach (ModelMesh mesh in model.Meshes)
            {
                //load textures
                foreach (BasicEffect local_effect in mesh.Effects)
                {
                    textures[i++] = local_effect.Texture;
                }

                //load effect
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = effect.Clone(device);
                }
            }

            //completedBoundingSphere will cover all the meshes's bounding sphere
            BoundingSphere completedBoundingSphere = new BoundingSphere();

            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere meshBoundingSphere = mesh.BoundingSphere;
                completedBoundingSphere = BoundingSphere.CreateMerged(completedBoundingSphere, meshBoundingSphere);
            }

            model.Tag = completedBoundingSphere;
        }

        // Transform bounding sphere
        public static BoundingSphere TransformBoundingSphere(BoundingSphere boundingSphere, Matrix world)
        {
            Vector3 trans;
            Vector3 scale;
            Quaternion quaternion;
            world.Decompose(out scale, out quaternion, out trans);

            float maxScale = scale.X;
            if (maxScale < scale.Y)
                maxScale = scale.Y;
            if (maxScale < scale.Z)
                maxScale = scale.Z;

            float transformedSphereRadius = boundingSphere.Radius * maxScale;
            Vector3 transformedSphereCenter = Vector3.Transform(boundingSphere.Center, world);

            BoundingSphere transformedBoundingSphere = new BoundingSphere(transformedSphereCenter, transformedSphereRadius);
            return transformedBoundingSphere;
        }

        // Draw bounding sphere
        //public static DrawBoundingSphere(BoundingSphere boundingSphere, Model refSphere, Matrix view, Matrix projection, Matrix world, Color color,
        //    GraphicsDevice device)
        //{

        //}

        // Draw model (no light)
        public static void DrawModel_noLight(ref Model model, Matrix view, Matrix projection, Matrix world)
        {
            Matrix[] model_transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(model_transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = model_transforms[mesh.ParentBone.Index] * world;
                    effect.LightingEnabled = false;
                }
                mesh.Draw();
            }
        }

        //Draw (with default light)
        public static void DrawModel_defaultLight(ref Model model, Matrix view, Matrix projection, Matrix world)
        {
            Matrix[] model_transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(model_transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = model_transforms[mesh.ParentBone.Index] * world;
                    effect.EnableDefaultLighting();
                }
                mesh.Draw();
            }
        }

        //Draw (with light adjust)
        //Note: High of lagging if drawing the model with textures

        public static void DrawModel_basicEffect(GraphicsDevice device, ContentManager Content, ref Model model, bool attchedWithTextures, ref BasicEffect basicEffect, Matrix view, Matrix projection, Matrix world)
        {
            Matrix[] model_transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(model_transforms);

            //Load textures
            Texture2D[] textures = new Texture2D[model.Meshes.Count];
            if (attchedWithTextures)
                LoadModel(device, Content, ref model, ref basicEffect, out textures);
            else
                LoadModel(device, Content, ref model, ref basicEffect);

            int i = 0;

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = model_transforms[mesh.ParentBone.Index] * world;
                    effect.AmbientLightColor = LightSettings.AmbientLightColor * LightSettings.LightPower * LightSettings.ModelLightFactor;
                    effect.DirectionalLight0.DiffuseColor = Color.White.ToVector3();
                    effect.DirectionalLight0.Direction = LightSettings.LightLookAt - LightSettings.LightPosition;
                    effect.DirectionalLight0.Enabled = LightSettings.LightEnabled;
                    if (attchedWithTextures)
                    {
                        effect.TextureEnabled = true;
                        effect.Texture = textures[i++];
                    }

                    //specular lighting for light source
                    if (LightSettings.LightSpecularColorCustomEnabled)
                    {
                        effect.DirectionalLight0.SpecularColor = LightSettings.LightSpecularColor * LightSettings.LightSpecularPower;
                    }

                    //specular lighting for object
                    if (LightSettings.SpecularColorCustomEnabled)
                    {
                        effect.SpecularColor = LightSettings.SpecularColor;
                        effect.SpecularPower = LightSettings.SpecularPower;
                    }

                    effect.PreferPerPixelLighting = LightSettings.PerPixelLightingEnabled;
                    effect.VertexColorEnabled = !attchedWithTextures;
                }
                mesh.Draw();
            }
        }

        //Draw (with the specified effect technique name and that model must have texture(s) attached to it.)
        //Precondition: Model should be loaded with the effect already.
        //Light properties should be set properly before call this method.
        //textures - must be set.
        //Note: The running of the textures's index will be initially set at zero and run until the end of mesh without any checking for out of bound.
        public static void DrawModel_withTechniqueTextured(ref Model model, Matrix view, Matrix projection, Matrix world, string technique, ref Texture2D[] textures)
        {
            //check first if the technique and textures are not set
            if (technique == "" || technique == null || textures == null || textures.Length <= 0)
                return;

            //Copy the transformation matrix from the model
            Matrix[] model_transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(model_transforms);

            //texture index-running
            int i = 0;

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (Effect effect in mesh.Effects)
                {
                    effect.CurrentTechnique = effect.Techniques[technique];
                    effect.Parameters["xView"].SetValue(view);
                    effect.Parameters["xProjection"].SetValue(projection);
                    effect.Parameters["xWorld"].SetValue(model_transforms[mesh.ParentBone.Index] * world);
                    effect.Parameters["xLightEnabled"].SetValue(LightSettings.LightEnabled);
                    effect.Parameters["xLightPosition"].SetValue(LightSettings.LightPosition);
                    effect.Parameters["xAmbient"].SetValue(LightSettings.LightAmbient);
                    effect.Parameters["xModelLightLevelPower"].SetValue(LightSettings.ModelLightFactor);
                    effect.Parameters["xLightPower"].SetValue(LightSettings.LightPower);
                    effect.Parameters["xSolidBrown"].SetValue(false);
                    effect.Parameters["xTexture"].SetValue(textures[i++]);
                }
                mesh.Draw();
            }
        }

        //Draw the model with the predefined color
        public static void DrawModel_withTechniqueColored(ref Model model, Matrix view, Matrix projection, Matrix world, string technique)
        {
            //Copy the transformation matrix from the model
            Matrix[] model_transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(model_transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (Effect effect in mesh.Effects)
                {
                    effect.CurrentTechnique = effect.Techniques[technique];
                    effect.Parameters["xView"].SetValue(view);
                    effect.Parameters["xProjection"].SetValue(projection);
                    effect.Parameters["xWorld"].SetValue(model_transforms[mesh.ParentBone.Index] * world);
                    effect.Parameters["xLightEnabled"].SetValue(LightSettings.LightEnabled);
                    effect.Parameters["xLightPosition"].SetValue(LightSettings.LightPosition);
                    effect.Parameters["xLightDirection"].SetValue(LightSettings.LightLookAt - LightSettings.LightPosition);
                    effect.Parameters["xAmbient"].SetValue(LightSettings.LightAmbient);
                    effect.Parameters["xModelLightLevelPower"].SetValue(LightSettings.ModelLightFactor);
                    effect.Parameters["xLightPower"].SetValue(LightSettings.LightPower);
                    effect.Parameters["xSolidBrown"].SetValue(false);
                }
                mesh.Draw();
            }
        }

        // Coarse collision checking for two specified models and their world matrix
        // Note: Use for same models
        // Remember: BoundingSphere is not automatically updated its world transform matrix
        public static bool CoarseCollisionCheck(Model model, Matrix world1, Matrix world2)
        {
            BoundingSphere sphere1 = (BoundingSphere)model.Tag;
            BoundingSphere sphere2 = (BoundingSphere)model.Tag;

            sphere1 = XNAUtil.TransformBoundingSphere(sphere1, world1);
            sphere2 = XNAUtil.TransformBoundingSphere(sphere2, world2);

            return sphere1.Intersects(sphere2);
        }

        // Coarse collision checking for two specified models and their world matrix
        // Note: Use for the models that are not the same
        // Remember: BoundingSphere is not automatically updated its world transform matrix
        public static bool CoarseCollisionCheck(Model model1, Matrix world1, Model model2, Matrix world2)
        {
            BoundingSphere sphere1 = (BoundingSphere)model1.Tag;
            BoundingSphere sphere2 = (BoundingSphere)model2.Tag;

            sphere1 = XNAUtil.TransformBoundingSphere(sphere1, world1);
            sphere2 = XNAUtil.TransformBoundingSphere(sphere2, world2);

            return sphere1.Intersects(sphere2);
        }

        // Finer collision checking for two specified models and their world matrix
        // Note: Use for same model
        // Remember: Matrix here is obtain from the model itself which is automatically updated every frame via the call of Update() method.
        //           So no need to multiply it with world matrix again.
        public static bool FinerCollisionCheck(Model model, Matrix world1, Matrix world2)
        {
            if (CoarseCollisionCheck(model, world1, world2) == false)
                return false;

            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere sphere1 = mesh.BoundingSphere;
                BoundingSphere sphere2 = mesh.BoundingSphere;

                Matrix tran1 = transforms[mesh.ParentBone.Index];
                Matrix tran2 = transforms[mesh.ParentBone.Index];

                BoundingSphere transformedSphere1 = XNAUtil.TransformBoundingSphere(sphere1, tran1);
                BoundingSphere transformedSphere2 = XNAUtil.TransformBoundingSphere(sphere2, tran2);

                if (transformedSphere1.Intersects(transformedSphere2))
                    return true;
            }

            return false;
        }

        // Finer collision checking for two specified models and their world matrix
        // Note: Use for same model
        // This is the version for those objects that have no update method.
        public static bool FinerCollisionCheck_ww(Model model, Matrix world1, Matrix world2)
        {
            if (CoarseCollisionCheck(model, world1, world2) == false)
                return false;

            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere sphere1 = mesh.BoundingSphere;
                BoundingSphere sphere2 = mesh.BoundingSphere;

                Matrix tran1 = transforms[mesh.ParentBone.Index] * world1;
                Matrix tran2 = transforms[mesh.ParentBone.Index] * world2;

                BoundingSphere transformedSphere1 = XNAUtil.TransformBoundingSphere(sphere1, tran1);
                BoundingSphere transformedSphere2 = XNAUtil.TransformBoundingSphere(sphere2, tran2);

                if (transformedSphere1.Intersects(transformedSphere2))
                    return true;
            }

            return false;
        }

        // Finer collision checking for two specified models and their world matrix
        // Note: Use for different model
        public static bool FinerCollisionCheck(Model model1, Matrix world1, Model model2, Matrix world2)
        {
            if (CoarseCollisionCheck(model1, world1, model2, world2) == false)
                return false;

            Matrix[] model1Transforms = new Matrix[model1.Bones.Count];
            Matrix[] model2Transforms = new Matrix[model2.Bones.Count];
            model1.CopyAbsoluteBoneTransformsTo(model1Transforms);
            model2.CopyAbsoluteBoneTransformsTo(model2Transforms);
            foreach (ModelMesh mesh1 in model1.Meshes)
            {
                BoundingSphere origSphere1 = mesh1.BoundingSphere;
                Matrix trans1 = model1Transforms[mesh1.ParentBone.Index];
                BoundingSphere transSphere1 = XNAUtil.TransformBoundingSphere(origSphere1, trans1);

                foreach (ModelMesh mesh2 in model2.Meshes)
                {
                    BoundingSphere origSphere2 = mesh2.BoundingSphere;
                    Matrix trans2 = model2Transforms[mesh2.ParentBone.Index];
                    BoundingSphere transSphere2 = XNAUtil.TransformBoundingSphere(origSphere2, trans2);

                    if (transSphere1.Intersects(transSphere2))
                        return true;
                }
            }
            return false;
        }

        // Finer collision checking for two specified models and their world matrix
        // Note: Use for different model
        // This is the version for those objects that have no update method.
        public static bool FinerCollisionCheck_ww(Model model1, Matrix world1, Model model2, Matrix world2)
        {
            if (CoarseCollisionCheck(model1, world1, model2, world2) == false)
                return false;

            Matrix[] model1Transforms = new Matrix[model1.Bones.Count];
            Matrix[] model2Transforms = new Matrix[model2.Bones.Count];
            model1.CopyAbsoluteBoneTransformsTo(model1Transforms);
            model2.CopyAbsoluteBoneTransformsTo(model2Transforms);
            foreach (ModelMesh mesh1 in model1.Meshes)
            {
                BoundingSphere origSphere1 = mesh1.BoundingSphere;
                Matrix trans1 = model1Transforms[mesh1.ParentBone.Index] * world1;
                BoundingSphere transSphere1 = XNAUtil.TransformBoundingSphere(origSphere1, trans1);

                foreach (ModelMesh mesh2 in model2.Meshes)
                {
                    BoundingSphere origSphere2 = mesh2.BoundingSphere;
                    Matrix trans2 = model2Transforms[mesh2.ParentBone.Index] * world2;
                    BoundingSphere transSphere2 = XNAUtil.TransformBoundingSphere(origSphere2, trans2);

                    if (transSphere1.Intersects(transSphere2))
                        return true;
                }
            }
            return false;
        }
    }
}
