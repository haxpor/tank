﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TankMovementAnimation
{
    //Use to set the level of brightness of such a model.
    class ModelLightLevel
    {
        float modelLightFactor;

        public float ModelLightFactor
        {
            get { return modelLightFactor; }
            set { modelLightFactor = value; }
        }
    }
}
