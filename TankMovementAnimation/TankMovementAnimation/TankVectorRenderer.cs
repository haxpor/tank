﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

// Companion class for tank model, work only with Tank class
// Note: The drawing process is designed to work for the tank with the scale of 0.0045f for all the axises
namespace TankMovementAnimation
{
    class TankVectorRenderer
    {
        //Reference the spritebatch from the caller
        SpriteBatch refSpriteBatch;

        //Reference the graphics device from the caller
        GraphicsDevice device;

        //All the temporary vertices used to draw the lines
        VertexPositionColor[] vertices;
        VertexDeclaration vertexDeclaration;

        //Reference effect from the caller
        Effect effect;

        //Constructor
        public TankVectorRenderer(SpriteBatch spriteBatch, GraphicsDevice device, Effect effect)
        {
            refSpriteBatch = spriteBatch;
            this.device = device;

            vertices = new VertexPositionColor[2];
            vertexDeclaration = new VertexDeclaration(this.device, VertexPositionColor.VertexElements);
            this.effect = effect;
        }

        // Draw the look forward vector
        public void DrawLookForwardVector(Vector3 tankPosition, Vector3 tankLookForward, Vector3 tankUp, Matrix view, Matrix projection)
        {
            Vector3 begin = tankPosition;
            begin += (1.55f * tankUp);
            //the begin of the line vector
            vertices[0] = new VertexPositionColor(begin, Color.Yellow);

            Vector3 end = tankLookForward;
            end *= 2.0f;
            end += tankPosition;
            end += (1.55f * tankUp);
            vertices[1] = new VertexPositionColor(end, Color.Yellow);

            //can use BasicEffect here (but we use our own effect file for clarity)
            effect.CurrentTechnique = effect.Techniques["Line"];
            effect.Parameters["xView"].SetValue(view);
            effect.Parameters["xProjection"].SetValue(projection);
            effect.Parameters["xWorld"].SetValue(Matrix.Identity);

            effect.Begin();
            effect.CurrentTechnique.Passes[0].Begin();

            device.VertexDeclaration = vertexDeclaration;
            device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, 1);

            effect.CurrentTechnique.Passes[0].End();
            effect.End();
        }

        // Draw the up vector
        public void DrawUpVector(Vector3 tankPosition, Vector3 tankUp, Matrix view, Matrix projection)
        {
            Vector3 begin = tankPosition;
            vertices[0] = new VertexPositionColor(begin, Color.Blue);

            Vector3 end = tankUp;
            end *= 2.0f;
            end += tankPosition;
            vertices[1] = new VertexPositionColor(end, Color.Blue);

            //can use BasicEffect here (but we use our own effect file for clarity)
            effect.CurrentTechnique = effect.Techniques["Line"];
            effect.Parameters["xView"].SetValue(view);
            effect.Parameters["xProjection"].SetValue(projection);
            effect.Parameters["xWorld"].SetValue(Matrix.Identity);

            effect.Begin();
            effect.CurrentTechnique.Passes[0].Begin();

            device.VertexDeclaration = vertexDeclaration;
            device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, 1);

            effect.CurrentTechnique.Passes[0].End();
            effect.End();
        }

        // Draw the cannon vector
        public void DrawCannonVector(Vector3 tankPosition, Vector3 tankCannon, Vector3 tankUp, Matrix view, Matrix projection)
        {
            Vector3 begin = tankPosition;
            begin += (1.65f * tankUp);
            vertices[0] = new VertexPositionColor(begin, Color.White);

            Vector3 end = tankCannon;
            end *= 2.0f;
            end += tankPosition;
            //end.Y += 1.65f;
            end += (1.65f * tankUp);
            vertices[1] = new VertexPositionColor(end, Color.White);

            //can use BasicEffect here (but we use our own effect file for clarity)
            effect.CurrentTechnique = effect.Techniques["Line"];
            effect.Parameters["xView"].SetValue(view);
            effect.Parameters["xProjection"].SetValue(projection);
            effect.Parameters["xWorld"].SetValue(Matrix.Identity);

            effect.Begin();
            effect.CurrentTechnique.Passes[0].Begin();

            device.VertexDeclaration = vertexDeclaration;
            device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, 1);

            effect.CurrentTechnique.Passes[0].End();
            effect.End();
        }

        // Draw the cannon vector
        public void DrawCannonBulletDirVector(Vector3 tankPosition, Vector3 tankCannonBulletDir, Vector3 tankUp, Matrix view, Matrix projection)
        {
            Vector3 begin = tankPosition;
            begin += (1.34f * tankUp);
            vertices[0] = new VertexPositionColor(begin, Color.Red);

            Vector3 end = tankCannonBulletDir;
            end *= 4.0f;
            end += (1.34f * tankUp);
            end += tankPosition;
            vertices[1] = new VertexPositionColor(end, Color.Red);

            //can use BasicEffect here (but we use our own effect file for clarity)
            effect.CurrentTechnique = effect.Techniques["Line"];
            effect.Parameters["xView"].SetValue(view);
            effect.Parameters["xProjection"].SetValue(projection);
            effect.Parameters["xWorld"].SetValue(Matrix.Identity);

            effect.Begin();
            effect.CurrentTechnique.Passes[0].Begin();

            device.VertexDeclaration = vertexDeclaration;
            device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, 1);

            effect.CurrentTechnique.Passes[0].End();
            effect.End();
        }
    }
}
