﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace TankMovementAnimation
{
    class SkyDome
    {
        GraphicsDevice device;

        Model skydome;

        Texture2D cloudStaticMap;
        Texture2D cloudMapTexture;
        RenderTarget2D cloudsRenderTarget;
        Effect effect;

        VertexPositionTexture[] fullScreenVertices;
        VertexDeclaration fullScreenVertexDeclaration;

        const int staticMapResolution = 32;

        //Constructor
        public SkyDome(string modelFile, ContentManager Content, GraphicsDevice device, Effect effect)
        {
            skydome = Content.Load<Model>(modelFile);
            skydome.Meshes[0].MeshParts[0].Effect = effect.Clone(device);

            this.effect = effect;
            this.device = device;
            cloudsRenderTarget = new RenderTarget2D(device, device.PresentationParameters.BackBufferWidth, device.PresentationParameters.BackBufferHeight, 
                1, device.DisplayMode.Format);

            //Create the static map to be used in perlin noise generation
            CreateStaticMap(staticMapResolution);
            //Create the full screen vertices
            fullScreenVertices = new VertexPositionTexture[4];
            SetUpFullscreenVertices();
            //Set the vertex declaration
            fullScreenVertexDeclaration = new VertexDeclaration(device, VertexPositionTexture.VertexElements);
        }

        // Set up the full screen vertices
        private void SetUpFullscreenVertices()
        {
            fullScreenVertices[0] = new VertexPositionTexture(new Vector3(-1, 1, 0f), new Vector2(0, 1));
            fullScreenVertices[1] = new VertexPositionTexture(new Vector3(1, 1, 0f), new Vector2(0, 0));
            fullScreenVertices[2] = new VertexPositionTexture(new Vector3(-1, -1, 0f), new Vector2(1, 1));
            fullScreenVertices[3] = new VertexPositionTexture(new Vector3(1, -1, 0f), new Vector2(1, 0));
        }

        // Create the static map
        private void CreateStaticMap(int resolution)
        {
            Random random = new Random((int)DateTime.Now.Ticks);
            Color[] noisyColors = new Color[resolution * resolution];
            for (int x = 0; x < resolution; x++)
            {
                for (int y = 0; y < resolution; y++)
                {
                    float value = (float)random.Next(1000) / 1000.0f;
                    noisyColors[x + y * resolution] = new Color(value, value, value);   //only black and white (beware the inverse in HLSL)
                }
            }

            Texture2D noiseImage = new Texture2D(device, resolution, resolution, 1, TextureUsage.None, SurfaceFormat.Color);
            noiseImage.SetData(noisyColors);
            cloudStaticMap = noiseImage;
        }

        // Generate static perlin noise 
        public void GeneratePerlinNoise()
        {
            device.SetRenderTarget(0, cloudsRenderTarget);
            device.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 1.0f, 0);

            effect.CurrentTechnique = effect.Techniques["PerlinNoiseStatic"];
            effect.Parameters["xStaticMap"].SetValue(cloudStaticMap);
            effect.Parameters["xOvercast"].SetValue(0.68f);
            effect.Begin();

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();

                device.VertexDeclaration = fullScreenVertexDeclaration;
                device.DrawUserPrimitives(PrimitiveType.TriangleStrip, fullScreenVertices, 0, 2);

                pass.End();
            }

            effect.End();

            device.SetRenderTarget(0, null);
            cloudMapTexture = cloudsRenderTarget.GetTexture();
            //cloudMapTexture.Save("cloudStaticMap.jpg", ImageFileFormat.Jpg);
        }

        // Generate the perline noise texture
        public void GeneratePerlinNoise(float time)
        {
            device.SetRenderTarget(0, cloudsRenderTarget);
            device.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 1.0f, 0);

            effect.CurrentTechnique = effect.Techniques["PerlinNoise"];
            effect.Parameters["xStaticMap"].SetValue(cloudStaticMap);
            effect.Parameters["xTime"].SetValue(time / 1500.0f);
            effect.Parameters["xOvercast"].SetValue(0.68f);
            effect.Begin();

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();

                device.VertexDeclaration = fullScreenVertexDeclaration;
                device.DrawUserPrimitives(PrimitiveType.TriangleStrip, fullScreenVertices, 0, 2);

                pass.End();
            }

            effect.End();

            device.SetRenderTarget(0, null);
            cloudMapTexture = cloudsRenderTarget.GetTexture();
            //cloudMapTexture.Save("cloudStaticMap.jpg", ImageFileFormat.Jpg);
        }

        public void Draw(GameTime gameTime, Vector3 cameraPosition, Matrix view, Matrix projection, Vector4 baseColor, Vector4 topColor)
        {
            device.RenderState.DepthBufferEnable = false;
            device.RenderState.DepthBufferWriteEnable = false;

            Matrix[] transforms = new Matrix[skydome.Bones.Count];
            skydome.CopyAbsoluteBoneTransformsTo(transforms);

            Matrix worldMatrix = Matrix.CreateTranslation(0.0f, -0.3f, 0.0f)
                    * Matrix.CreateScale(10.0f)
                    * Matrix.CreateTranslation(cameraPosition);

            foreach (ModelMesh mesh in skydome.Meshes)
            {
                worldMatrix = transforms[mesh.ParentBone.Index] * worldMatrix;

                foreach (Effect effect in mesh.Effects)
                {
                    effect.CurrentTechnique = effect.Techniques["SkyDome"];
                    effect.Parameters["xView"].SetValue(view);
                    effect.Parameters["xProjection"].SetValue(projection);
                    effect.Parameters["xWorld"].SetValue(worldMatrix);
                    effect.Parameters["xLightEnabled"].SetValue(false); //always false
                    effect.Parameters["xTexture"].SetValue(cloudMapTexture);
                    effect.Parameters["xSkyDomeBaseColor"].SetValue(baseColor);
                    effect.Parameters["xSkyDomeTopColor"].SetValue(topColor);
                }

                mesh.Draw();
            }

            device.RenderState.DepthBufferEnable = true;
            device.RenderState.DepthBufferWriteEnable = true;
        }
    }
}
