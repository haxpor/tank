using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace TankMovementAnimation
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        GraphicsDevice device;
        SpriteBatch spriteBatch;
        SpriteFont myFont;

        //Effect files
        Effect effect;
        Effect tankLineVectorEffect;

        //Object in game
        Model tankModel;
        Tank tank;
        Terrain terrain;
        SkyDome skyDome;
        TankVectorRenderer tankVectorRenderer;

        //Camera stuff
        Matrix view;
        Matrix projection;
        Vector3 camPos;
        Vector3 camLookDirection;
        Vector3 camUp;
        Matrix camRotation = Matrix.Identity;

        //Tank camera control
        float camZ_zoomAmount;
        float camY_zoomAmount;
        MouseState preCamMouseState;

        //Mouse stuff
        MouseManager mouseRel;

        float preTankHeight;
        bool isFreeMode;
        bool fireMode;

        KeyboardState preKeyState;

        bool fullScreen = false;
        bool isCouldMoving = true;

        //Shadow Mapping Texture Generating
        RenderTarget2D shadowMapRenderTarget;
        Texture2D shadowMapTexture;
        Vector3 lightPosition = new Vector3(15f, 10.0f, 50f);
        Vector3 lightLookAt = new Vector3(3, -5, 10);
        float lightPower = 0.45f;
        float lightAmbient = 0.34f;
        Matrix lightView;
        Matrix lightProjection;

        //Skydome environment
        Vector4 skyBaseColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
        Vector4 skyTopColor = new Vector4(0.2f, 0.2f, 0.5f, 1.0f);

        //Light

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;
            if (fullScreen)
            {
                graphics.PreferredBackBufferWidth = 1024;
                graphics.PreferredBackBufferHeight = 768;
                graphics.ToggleFullScreen();
                graphics.ApplyChanges();
            }
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            //create our shortcut to graphics device
            device = graphics.GraphicsDevice;

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            myFont = Content.Load<SpriteFont>("myFont");

            // TODO: use this.Content to load your game content here
            effect = Content.Load<Effect>("effect");
            tankLineVectorEffect = Content.Load<Effect>("LineEffect");

            tankModel = Content.Load<Model>("tank");
            tank = new Tank(ref tankModel, Content, device, ref effect, true, true);
            tank.Position = new Vector3(0, 0, 10.0f);
            terrain = new Terrain("heightmap", Content, device, ref effect);
            terrain.Load();
            tankVectorRenderer = new TankVectorRenderer(spriteBatch, device, tankLineVectorEffect);

            skyDome = new SkyDome("dome", Content, device, effect);

            //set up mouse relative
            mouseRel = new MouseManager(device.DisplayMode.Width, device.DisplayMode.Height);
            mouseRel.AlwaysKeepMouseAtCenter = true;

            //generate the static perlin noise
            skyDome.GeneratePerlinNoise();

            //Set the previous cam mouse state
            preCamMouseState = Mouse.GetState();

            //Set the previous keyboard state
            preKeyState = Keyboard.GetState();

            //For generate the shadow map
            PresentationParameters pp = device.PresentationParameters;
            shadowMapRenderTarget = new RenderTarget2D(device, pp.BackBufferWidth, pp.BackBufferHeight, 1, device.DisplayMode.Format);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            float diffTime = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f;
            float totalTime = (float)gameTime.TotalGameTime.TotalMilliseconds / 100.0f;
            KeyboardState keyState = Keyboard.GetState();

            // Allows the game to exit
            if (keyState.IsKeyDown(Keys.Escape))
                this.Exit();

            //process tank's movement
            ProcessTankControlCenter(gameTime, keyState);
            ProcessTankMovement(gameTime, keyState);
            ProcessTankCam(gameTime, keyState);

            Matrix tankWorld = Matrix.CreateScale(0.0045f) * Matrix.CreateRotationY(MathHelper.Pi);
            tank.CalculateRealisticMovementOnTerrainSurface(ref terrain, tankWorld);
            tank.Update(gameTime, tankWorld);

            //Update the relative mouse movement
            mouseRel.MouseRelativeEnabled = isFreeMode;
            mouseRel.Update(diffTime);

            //Update tank height
            UpdateTankHeight();

            //Update camera position
            UpdateCamera();

            //Update cloud
            if(isCouldMoving)
                skyDome.GeneratePerlinNoise(totalTime);

            base.Update(gameTime);
        }

        private void UpdateTankHeight()
        {
            float currentHeight = terrain.GetHeight(tank.Position);

            if (currentHeight != preTankHeight)
            {
                float amount = (float)Math.Tan((float)Math.Atan2(currentHeight, preTankHeight)) % 1;
                float height = MathHelper.Lerp(preTankHeight, currentHeight, amount);
                tank.Height = height;

                preTankHeight = currentHeight;
            }
        }

        private void UpdateCamera()
        {
            //reposition the camera to a little bit behind and above the tank
            float zCamCompare = 2.0f;
            float yCamCompare = 2.25f;

            camPos = new Vector3(0, 2.4f + camY_zoomAmount, 6.0f + camZ_zoomAmount);
            if (camPos.Z < zCamCompare)
                camPos.Z = zCamCompare;
            if (camPos.Y < yCamCompare)
                camPos.Y = yCamCompare;

            //camera delay
            if(fireMode)
                camRotation = Matrix.Lerp(camRotation, tank.CannonRotation, 0.1f);
            else
                camRotation = Matrix.Lerp(camRotation, tank.TankRotation, 0.1f);
            camPos = Vector3.Transform(camPos, camRotation);
            camPos += tank.Position;

            //mouse look
            Matrix leftRight = Matrix.CreateRotationY(mouseRel.CameraRotX);
            Matrix upDown = Matrix.CreateRotationX(mouseRel.CameraRotY);
            Matrix rotatedMouseLook = upDown * leftRight;

            //update camera's look vector
            camLookDirection = Vector3.Zero;
            if (isFreeMode)
                camLookDirection = Vector3.Transform(new Vector3(0, 0, -1), rotatedMouseLook);
            else
                camLookDirection = Vector3.Transform(new Vector3(0, 0, -1), camRotation);
            camLookDirection += camPos;

            //update camera's up vector
            if(isFreeMode)
                camUp = Vector3.Transform(new Vector3(0, 1, 0), rotatedMouseLook);
            else
                camUp = Vector3.Transform(new Vector3(0, 1, 0), tank.TankRotation);
            //camUp = Vector3.Transform(tank.UpVector, rotatedMouseLook);

            view = Matrix.CreateLookAt(camPos, camLookDirection, camUp);
            projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, device.DisplayMode.AspectRatio, 1.0f, 1000.0f);

            //update light camera
            lightView = Matrix.CreateLookAt(lightPosition, lightLookAt, Vector3.Up);
            lightProjection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver2, device.DisplayMode.AspectRatio, 1.0f, 1000.0f);
        }

        private void ProcessTankControlCenter(GameTime gameTime, KeyboardState currkey)
        {
            //enable or disable freemode
            if (currkey.IsKeyDown(Keys.Tab) && preKeyState.IsKeyUp(Keys.Tab))
            {
                isFreeMode = !isFreeMode;
                if (!isFreeMode)
                {
                    //clear all the zoom amount
                    camY_zoomAmount = 0.0f;
                    camZ_zoomAmount = 0.0f;
                }
            }

            if (mouseRel.MiddleMouseClicked() || (currkey.IsKeyDown(Keys.LeftAlt) && preKeyState.IsKeyUp(Keys.LeftAlt)))
                fireMode = !fireMode;

            //update the previous
            preKeyState = currkey;
        }

        private void ProcessTankMovement(GameTime gameTime, KeyboardState key)
        {
            //tank forward
            if (key.IsKeyDown(Keys.W))
            {
                tank.BackWheelRotationAngle = 0.07f;
                tank.FrontWheelRotationAngle = 0.1f;
                tank.MoveForward(0.045f);
            }
            //move steer to the normal direction
            if (key.IsKeyUp(Keys.A) && key.IsKeyUp(Keys.D))
            {
                //return to the normal state
                if (key.IsKeyDown(Keys.W))
                    tank.SteerRotationAngle = 0.0f;
                if (key.IsKeyDown(Keys.S))
                    tank.SteerRotationAngle = 0.0f;
            }
            //tank backward
            if (key.IsKeyDown(Keys.S))
            {
                tank.BackWheelRotationAngle = -0.07f;
                tank.FrontWheelRotationAngle = -0.1f;
                tank.MoveBackward(0.045f);
            }

            //#Set 0 Left or right of the steering
            if(key.IsKeyUp(Keys.W) && key.IsKeyUp(Keys.S)){

                if(key.IsKeyDown(Keys.A))
                    tank.SteerRotationAngle = 0.02f;
                if(key.IsKeyDown(Keys.D))
                    tank.SteerRotationAngle = -0.02f;
            }

            //#Set 1 Realistic forward movement for steering
            //tank steer left
            if (key.IsKeyDown(Keys.A) && preKeyState.IsKeyDown(Keys.W))
            {
                tank.SteerRotationAngle = 0.02f;
                tank.RealisticManeuverAngle = 0.02f;
            }
            //tank steer right
            if (key.IsKeyDown(Keys.D) && preKeyState.IsKeyDown(Keys.W))
            {
                tank.SteerRotationAngle = -0.02f;
                tank.RealisticManeuverAngle = -0.02f;
            }

            //#Set 2 Realistic backward movement for steering
            //tank steer left
            if (key.IsKeyDown(Keys.A) && preKeyState.IsKeyDown(Keys.S))
            {
                tank.SteerRotationAngle = -0.02f;
                tank.RealisticManeuverAngle = 0.02f;
            }
            //tank steer right
            if (key.IsKeyDown(Keys.D) && preKeyState.IsKeyDown(Keys.S))
            {
                tank.SteerRotationAngle = 0.02f;
                tank.RealisticManeuverAngle = -0.02f;
            }

            //hatch down
            if (key.IsKeyDown(Keys.Z))
            {
                tank.HatchRotationAngle = 0.01f;
            }
            //hatch up
            if (key.IsKeyDown(Keys.X))
            {
                tank.HatchRotationAngle = -0.01f;
            }

            //cannon down
            if (key.IsKeyDown(Keys.F))
            {
                tank.CannonRotationAngle = 0.004f;
            }
            //cannon up
            if (key.IsKeyDown(Keys.R))
            {
                tank.CannonRotationAngle = -0.004f;
            }

            //turret rotate left
            if (key.IsKeyDown(Keys.Q))
            {
                tank.TurretRotationAngle = 0.02f;
            }
            //turret rotate right
            if (key.IsKeyDown(Keys.E))
            {
                tank.TurretRotationAngle = -0.02f;
            }
        }

        // Process tank's camera
        private void ProcessTankCam(GameTime gameTime, KeyboardState keyState)
        {
            if (isFreeMode)
            {
                camZ_zoomAmount += mouseRel.ScrollDiffAmount * -0.0043f;
                camY_zoomAmount += mouseRel.ScrollDiffAmount * -0.0043f;
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {         
            // TODO: Add your drawing code here
            
            //Draw with shadow map
            DrawShadowMap(gameTime);

            //Draw normal scene
            DrawNormalScene(gameTime);

            base.Draw(gameTime);
        }

        private void DrawNormalScene(GameTime gameTime)
        {
            //Draw with normal technique
            graphics.GraphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 1.0f, 0);
            skyDome.Draw(gameTime, camPos, view, projection, skyBaseColor, skyTopColor);

            terrain.LightSettings.LightEnabled = true;
            terrain.LightSettings.LightLookAt = lightLookAt;
            terrain.LightSettings.LightPosition = lightPosition;
            terrain.LightSettings.LightPower = lightPower;
            terrain.LightSettings.LightAmbient = lightAmbient;
            terrain.LightSettings.LightType = LightSettings.LIGHT_TYPE_POINTLIGHT;
            terrain.EffectSettings.eShadowedScene.Enabled = true;
            terrain.EffectSettings.eShadowedScene.BiasedDistance = 5; //terrain is squared in area
            terrain.EffectSettings.eShadowedScene.ShadowMap = shadowMapTexture;
            terrain.Draw("ShadowedSceneMultiTextured", gameTime, Matrix.Identity, view, projection);

            //tank
            tank.LightSettings.LightEnabled = true;
            tank.LightSettings.LightLookAt = lightLookAt;
            tank.LightSettings.AmbientLightColor = Color.Black.ToVector3();
            tank.LightSettings.AmbientLightColorFactor = 1.0f;
            tank.LightSettings.LightPosition = lightPosition;
            tank.LightSettings.LightPower = lightPower;
            tank.LightSettings.LightAmbient = lightAmbient;
            tank.EffectSettings.eShadowedScene.Enabled = true;
            tank.EffectSettings.eShadowedScene.BiasedDistance = 5; //terrain is squared in area
            tank.EffectSettings.eShadowedScene.ShadowMap = shadowMapTexture;
            tank.EffectSettings.eShadowedScene.BaseCustomDiffuseColorEnabled = true;
            tank.EffectSettings.eShadowedScene.BaseDiffuseColorFactor = 0.67f;
            tank.LightSettings.LightType = LightSettings.LIGHT_TYPE_POINTLIGHT;
            tank.Draw("ShadowedScene", gameTime, view, projection);

            tankVectorRenderer.DrawLookForwardVector(tank.Position, tank.LookForwardVector, tank.UpVector, view, projection);
            tankVectorRenderer.DrawUpVector(tank.Position, tank.UpVector, view, projection);
            tankVectorRenderer.DrawCannonVector(tank.Position, tank.CannonForwardVector, tank.UpVector, view, projection);
            tankVectorRenderer.DrawCannonBulletDirVector(tank.Position, tank.CannonBulletDirVector, tank.UpVector, view, projection);

            DrawText();
        }

        private void DrawShadowMap(GameTime gameTime)
        {
            //Generate the shadow map
            device.SetRenderTarget(0, shadowMapRenderTarget);
            graphics.GraphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 1.0f, 0);

            //terrain
            terrain.LightSettings.LightEnabled = true;
            terrain.LightSettings.LightLookAt = lightLookAt;
            terrain.LightSettings.LightPosition = lightPosition;
            terrain.LightSettings.LightPower = lightPower;
            terrain.LightSettings.LightAmbient = lightAmbient;
            terrain.LightSettings.LightView = lightView;
            terrain.LightSettings.LightProjection = lightProjection;
            terrain.Draw("ShadowMap", gameTime, Matrix.Identity, lightView, lightProjection);

            //tank
            tank.LightSettings.LightEnabled = true;
            tank.LightSettings.LightLookAt = lightLookAt;
            tank.LightSettings.LightPosition = lightPosition;
            tank.LightSettings.LightPower = lightPower;
            tank.LightSettings.LightAmbient = lightAmbient;
            tank.LightSettings.LightView = lightView;
            tank.LightSettings.LightProjection = lightProjection;
            tank.EffectSettings.eModel.ModelLightFactor = 0.67f;
            tank.Draw("ShadowMap", gameTime, lightView, lightProjection);
            device.SetRenderTarget(0, null);

            //save to the shadow texture
            shadowMapTexture = shadowMapRenderTarget.GetTexture();
            //shadowMapTexture.Save("shadowMap.jpg", ImageFileFormat.Jpg);
        }

        private void DrawText()
        {
            spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.SaveState, Matrix.Identity);
            //Position
            spriteBatch.DrawString(myFont, "X: " + tank.Position.X, new Vector2(0, 10), Color.Red);
            spriteBatch.DrawString(myFont, "Y: " + tank.Position.Y, new Vector2(0, 10 + 16), Color.Red);
            spriteBatch.DrawString(myFont, "Z: " + tank.Position.Z, new Vector2(0, 10 + 16 * 2), Color.Red);

            //Mode currently used
            string mode = "";
            if (isFreeMode && fireMode)
                mode = "Mixed";
            else if (isFreeMode && !fireMode)
                mode = "Navigate";
            else if (!isFreeMode && fireMode)
                mode = "Fight";
            else
                mode = "Forward";
            spriteBatch.DrawString(myFont, "Mode: " + mode, new Vector2(0, 10 + 16 * 3), Color.Red);
            spriteBatch.End();
        }
    }
}
