﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace TankMovementAnimation
{
    //Used to provide convenience in drawing the object while setting the parameters in effect file through this class instead of 
    // directly the drawing method of that object.

    //Note: Caller must choose only the intend to used struct to work with the apps.
    class EffectSettings
    {
        //For 'ShadowedScene' Technique
        public struct ShadowedScene
        {
            public bool Enabled;
            public float BiasedDistance;
            public Texture2D ShadowMap;

            //setting of base diffuse color factor of shadow
            public bool BaseCustomDiffuseColorEnabled;
            public float BaseDiffuseColorFactor;
        };
        public ShadowedScene eShadowedScene;

        //For 'Model' Technique
        public struct Model
        {
            public float ModelLightFactor;
        };
        public Model eModel;

        //Constructor
        public EffectSettings()
        {
            eModel.ModelLightFactor = 1.0f; //default
        }
    }
}
