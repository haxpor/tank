﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TankMovementAnimation
{
    //Information for the caller to set the light properties for the class that have it.
    class LightSettings
    {
        //Specification
        public const int LIGHT_TYPE_POINTLIGHT = 1;
        public const int LIGHT_TYPE_DIRECTIONAL_LIGHT = 2;
        public const int LIGHT_TYPE_SPOTLIGHT = 3;

        //General Light settings
        float lightAmbient = 1.0f;
        float lightPower = 1.0f;    //default
        Matrix lightView = Matrix.Identity;     //default: avoid error
        Matrix lightProjection = Matrix.Identity;   //default: avoid error
        Vector3 lightPosition;
        Vector3 lightLookAt;
        bool lightEnabled;
        bool preferPerPixelLighting = false;    //default is disable

        //Type of light source
        int lightType = 1;  //default is PointLight

        //Ambient light color
        Vector3 ambientLightColor = Vector3.Zero;   //use black color, due to the addition method used in the effect file
        float ambientLightColorFactor = 1.0f;     //how hard the ambient light color will dominant

        //Model light level factor
        float modelLightFactor = 1.0f;

        //Specular color (for object)
        bool specularColorCustomEnabled = false;    //enable the specular color customization or not
        Vector3 specularColor;
        float specularPower;

        //Specular color (for light source)
        bool lightSpecularColorCustomEnabled = false;
        Vector3 lightSpecularColor;
        float lightSpecularPower;

        //Only for SpotLight type
        float spotLightLimitAngle;
        Vector3 spotLightDirection;
        float spotLightDecay;

        //General light settings
        public float LightAmbient
        {
            get { return lightAmbient; }
            set { lightAmbient = value; }
        }
        public float LightPower
        {
            get { return lightPower; }
            set { lightPower = value; }
        }
        public Matrix LightView
        {
            get { return lightView; }
            set { lightView = value; }
        }
        public Matrix LightProjection
        {
            get { return lightProjection; }
            set { lightProjection = value; }
        }
        public Vector3 LightPosition
        {
            get { return lightPosition; }
            set { lightPosition = value; }
        }
        public Vector3 LightLookAt
        {
            get { return lightLookAt; }
            set { lightLookAt = value; }
        }
        public bool LightEnabled
        {
            get { return lightEnabled; }
            set { lightEnabled = value; }
        }
        public bool PerPixelLightingEnabled
        {
            get { return preferPerPixelLighting; }
            set { preferPerPixelLighting = value; }
        }

        //Type of light source
        public int LightType
        {
            get { return lightType; }
            set { lightType = value; }
        }

        //Ambient light color
        public Vector3 AmbientLightColor
        {
            get { return ambientLightColor; }
            set { ambientLightColor = value; }
        }
        public float AmbientLightColorFactor
        {
            get { return ambientLightColorFactor; }
            set { ambientLightColorFactor = value; }
        }

        //Model light level factor
        public float ModelLightFactor
        {
            get { return modelLightFactor; }
            set { modelLightFactor = value; }
        }

        //Specular color customization
        public bool SpecularColorCustomEnabled
        {
            get { return specularColorCustomEnabled; }
            set { specularColorCustomEnabled = value; }
        }
        public Vector3 SpecularColor
        {
            get { return specularColor; }
            set { specularColor = value; }
        }
        public float SpecularPower
        {
            get { return specularPower; }
            set { specularPower = value; }
        }

        //Light source specular customization
        public bool LightSpecularColorCustomEnabled
        {
            get { return lightSpecularColorCustomEnabled; }
            set { lightSpecularColorCustomEnabled = value; }
        }
        public Vector3 LightSpecularColor
        {
            get { return lightSpecularColor; }
            set { lightSpecularColor = value; }
        }
        public float LightSpecularPower
        {
            get { return lightSpecularPower; }
            set { lightSpecularPower = value; }
        }

        //Only for the settings of spotlight
        public float SpotLightLimitAngle
        {
            get { return spotLightLimitAngle; }
            set { spotLightLimitAngle = value; }
        }
        public Vector3 SpotLightDirection
        {
            get { return spotLightDirection; }
            set { spotLightDirection = value; }
        }
        public float SpotLightDecay
        {
            get { return spotLightDecay; }
            set { spotLightDecay = value; }
        }
    }
}
