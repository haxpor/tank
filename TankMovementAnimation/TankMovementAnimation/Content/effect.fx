//------- Constants --------
float4x4 xView;
float4x4 xProjection;
float4x4 xWorld;

float4x4 xLightView;
float4x4 xLightProjection;
float3 xLightDirection;
float3 xLightPosition;
float xAmbient;
float xLightPower;
bool xLightEnabled;

//Ambient light color
float3 xLightAmbientColor;
float xLightAmbientColorFactor;

//Model color (used if model has no textures)
bool xSolidBrown;

//Maximum distance of the scene (used in static shadow mapping)
float xBiasedDistance;

//ShadowedScene intensity of initial diffuseColorFactor setting
bool xEnabledShadowedSceneCustomInitDiffuseColorFactor;
float xBaseDiffuseColorFactor;

//Light type
//Constant for types of light
// PointLight = 1
// DirecitonalLight = 2
int xLightType;

//------- Texture Samplers --------
//general purpose texture
Texture xTexture;
sampler TextureSampler = sampler_state { texture = <xTexture>; magfilter = LINEAR; minfilter = LINEAR; mipfilter=LINEAR; AddressU = mirror; AddressV = mirror;};

//shadow map texture
Texture xShadowMap;
sampler ShadowMapSampler = sampler_state { texture = <xShadowMap>; magfilter = LINEAR; minfilter = LINEAR; mipfilter=NONE; AddressU = mirror; AddressV = mirror;};

//-- User Defined function --//
//Use only for the very static scene
float DotProduct(float3 lightPos, float3 pos3D, float3 normal)
{
	float3 lightDirection = normalize(pos3D - lightPos);
	return dot(normal, -lightDirection);
}

//------- Technique: Colored --------
struct ColVertexToPixel
{
    float4 Position   	: POSITION;    
    float4 Color		: COLOR0;
    float LightingFactor: TEXCOORD0;
};

struct ColPixelToFrame
{
    float4 Color : COLOR0;
};

ColVertexToPixel ColoredVS( float4 inPos : POSITION, float4 inColor: COLOR, float3 inNormal: NORMAL)
{	
	ColVertexToPixel Output = (ColVertexToPixel)0;
	float4x4 preViewProjection = mul (xView, xProjection);
	float4x4 preWorldViewProjection = mul (xWorld, preViewProjection);
    
	Output.Position = mul(inPos, preWorldViewProjection);
	Output.Color = inColor;
	
	float3 Normal = normalize(mul(normalize(inNormal), xWorld));	
	Output.LightingFactor = 1;
	if (xLightEnabled)
		Output.LightingFactor = saturate(dot(Normal, -xLightDirection));
    
	return Output;    
}

ColPixelToFrame ColoredPS(ColVertexToPixel PSIn) 
{
	ColPixelToFrame Output = (ColPixelToFrame)0;		
    
	Output.Color = PSIn.Color;
	Output.Color.rgb *= saturate(PSIn.LightingFactor + xAmbient);	
	
	return Output;
}

technique Colored
{
	pass Pass0
    {   
    	VertexShader = compile vs_2_0 ColoredVS();
        PixelShader  = compile ps_2_0 ColoredPS();
    }
}

//------- Technique: Textured --------
struct TexVertexToPixel
{
    float4 Position   	: POSITION;    
    float4 Color		: COLOR0;
    float LightingFactor: TEXCOORD0;
    float2 TextureCoords: TEXCOORD1;
};

struct TexPixelToFrame
{
    float4 Color : COLOR0;
};

TexVertexToPixel TexturedVS( float4 inPos : POSITION, float3 inNormal: NORMAL, float2 inTexCoords: TEXCOORD0)
{	
	TexVertexToPixel Output = (TexVertexToPixel)0;
	float4x4 preViewProjection = mul (xView, xProjection);
	float4x4 preWorldViewProjection = mul (xWorld, preViewProjection);
    
	Output.Position = mul(inPos, preWorldViewProjection);	
	Output.TextureCoords = inTexCoords;
	
	float3 Normal = normalize(mul(normalize(inNormal), xWorld));	
	Output.LightingFactor = 1;
	if (xLightEnabled)
		Output.LightingFactor = saturate(dot(Normal, -xLightDirection));
    
	return Output;     
}

TexPixelToFrame TexturedPS(TexVertexToPixel PSIn) 
{
	TexPixelToFrame Output = (TexPixelToFrame)0;		
    
	Output.Color = tex2D(TextureSampler, PSIn.TextureCoords);
	Output.Color.rgb *= saturate(PSIn.LightingFactor + xAmbient);

	return Output;
}

technique Textured
{
	pass Pass0
    {   
    	VertexShader = compile vs_2_0 TexturedVS();
        PixelShader  = compile ps_2_0 TexturedPS();
    }
}

//-- MultiTextured Technique --//
// -- Texture Sampler -- //
Texture xTexture0;
Texture xTexture1;
Texture xTexture2;
Texture xTexture3;

sampler TextureSampler0 = sampler_state { texture = <xTexture0>; magfilter=LINEAR; minfilter=LINEAR; mipfilter=LINEAR;
AddressU=wrap; AddressV=wrap; };

sampler TextureSampler1 = sampler_state { texture = <xTexture1>; magfilter=LINEAR; minfilter=LINEAR; mipfilter=LINEAR;
AddressU=wrap; AddressV=wrap; };

sampler TextureSampler2 = sampler_state { texture = <xTexture2>; magfilter=LINEAR; minfilter=LINEAR; mipfilter=LINEAR;
AddressU=mirror; AddressV=mirror; };

sampler TextureSampler3 = sampler_state { texture = <xTexture3>; magfilter=LINEAR; minfilter=LINEAR; mipfilter=LINEAR;
AddressU=mirror; AddressV=mirror; };

struct MTVertexToPixel
{
	float4 Position		: POSITION;
	float4 Color		: COLOR0;
	float3 Normal		: TEXCOORD0;
	float2 TextureCoords: TEXCOORD1;
	float4 TextureWeights : TEXCOORD2;
	float Depth	: TEXCOORD3;
	float4 Position3D	: TEXCOORD4;
};

struct MTPixelToFrame
{
	float4 Color	: COLOR0;
};

MTVertexToPixel MTVertexShader(float4 inPos:POSITION, float3 inNormal:NORMAL, float2 inTextureCoords:TEXCOORD0, float4 inTextureWeights:TEXCOORD1)
{
	MTVertexToPixel Output = (MTVertexToPixel)0;
	
	float4x4 preViewProjection = mul(xView, xProjection);
	float4x4 preWorldViewProjection = mul(xWorld, preViewProjection);
	
	Output.Position = mul(inPos, preWorldViewProjection);
	Output.Normal = mul(normalize(inNormal), xWorld);
	Output.TextureCoords = inTextureCoords;
	Output.TextureWeights = inTextureWeights;
	Output.Depth = Output.Position.z/Output.Position.w;
	Output.Position3D = mul(inPos, xWorld);
	
	return Output;
}

MTPixelToFrame MTPixelShader(MTVertexToPixel inMT)
{
	MTPixelToFrame Output = (MTPixelToFrame)0;
	
	float lightFactor = 1.0f;
	if(xLightEnabled)
	{
		//lightFactor = saturate(saturate(dot(inMT.Normal, inMT.LightDirection)) + xAmbient);
		if(xLightType == 1)
			lightFactor = saturate(DotProduct(xLightPosition, inMT.Position3D, inMT.Normal));
		else if(xLightType == 2)
			lightFactor = saturate(dot(inMT.Normal, -xLightDirection));
	}
		
	float depthDistance = 0.99f;
	float depthWidth = 0.005f;
	float blendFactor = clamp((inMT.Depth-depthDistance)/depthWidth, 0.0f, 1.0f);
	
	float4 farColor;
	farColor = tex2D(TextureSampler0, inMT.TextureCoords) * inMT.TextureWeights.x;
	farColor += tex2D(TextureSampler1, inMT.TextureCoords) * inMT.TextureWeights.y;
	farColor += tex2D(TextureSampler2, inMT.TextureCoords) * inMT.TextureWeights.z;
	farColor += tex2D(TextureSampler3, inMT.TextureCoords) * inMT.TextureWeights.w;
	
	float4 nearColor;
	float2 textureCoords = inMT.TextureCoords * 3.0f;
	nearColor = tex2D(TextureSampler0, textureCoords) * inMT.TextureWeights.x;
	nearColor += tex2D(TextureSampler1, textureCoords) * inMT.TextureWeights.y;
	nearColor += tex2D(TextureSampler2, textureCoords) * inMT.TextureWeights.z;
	nearColor += tex2D(TextureSampler3, textureCoords) * inMT.TextureWeights.w;
	
	Output.Color = lerp(nearColor, farColor, blendFactor);
	Output.Color *= (lightFactor + xAmbient);
	
	return Output;
}

technique MultiTextured
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 MTVertexShader();
		PixelShader = compile ps_2_0 MTPixelShader();
	}
}

//-- ShadowedSceneMultiTextured Technique --//
//Note: Use with your own risk, not all the graphics card support it.
struct ShadowedSceneMultiTexturedToPixel
{
	float4 Position		: POSITION;
	float4 Color		: COLOR0;
	float3 Normal		: TEXCOORD0;
	float2 TextureCoords: TEXCOORD1;
	float4 TextureWeights : TEXCOORD2;
	float Depth	: TEXCOORD3;
	float4 Position3D	: TEXCOORD4;
	float4 Position2D_SeenByLight : TEXCOORD5;
};

struct ShadowedSceneMultiTexturedToFrame
{
	float4 Color : COLOR;
};

ShadowedSceneMultiTexturedToPixel ShadowedSceneMultiTexturedVS(float4 inPos:POSITION, float3 inNormal:NORMAL, float2 inTextureCoords:TEXCOORD0, float4 inTextureWeights:TEXCOORD1)
{
	ShadowedSceneMultiTexturedToPixel Output = (ShadowedSceneMultiTexturedToPixel)0;
	
	float4x4 preViewProjection = mul(xView, xProjection);
	float4x4 preWorldViewProjection = mul(xWorld, preViewProjection);
	
	Output.Position = mul(inPos, preWorldViewProjection);
	Output.Normal = normalize(mul(inNormal, (float3x3)xWorld));
	Output.TextureCoords = inTextureCoords;
	Output.TextureWeights = inTextureWeights;
	Output.Depth = Output.Position.z/Output.Position.w;
	Output.Position3D = mul(inPos, xWorld);
	
	return Output;
}

ShadowedSceneMultiTexturedToFrame ShadowedSceneMultiTexturedPS(ShadowedSceneMultiTexturedToPixel inS)
{
	ShadowedSceneMultiTexturedToFrame Output = (ShadowedSceneMultiTexturedToFrame)0;
	
	//calculate the texture coordinate for the shadow map
	//Note: We must convert from [-1,1] to [0,1] due to we have passed the interpolation process.
	float2 projectedTexCoords;
	projectedTexCoords.x = inS.Position2D_SeenByLight.x / inS.Position2D_SeenByLight.w / 2.0f + 0.5f;
	projectedTexCoords.y = -inS.Position2D_SeenByLight.y / inS.Position2D_SeenByLight.w / 2.0f + 0.5f;
	
	//checking if the coordinate is in range of the view frustum
	float diffuseColorFactor = 0.0f;
	if(xEnabledShadowedSceneCustomInitDiffuseColorFactor)
		diffuseColorFactor = xBaseDiffuseColorFactor;
	if(saturate(projectedTexCoords.x) == projectedTexCoords.x && saturate(projectedTexCoords.y) == projectedTexCoords.y && xLightEnabled)
	{
		float depthStoredInMap = tex2D(ShadowMapSampler, projectedTexCoords).r;
		float realDistance = inS.Position2D_SeenByLight.z / inS.Position2D_SeenByLight.w;
		if(realDistance - 1.0f / xBiasedDistance <= depthStoredInMap)
		{
			//calculate the diffuse color factor
			if(xLightType == 1)
				diffuseColorFactor = DotProduct(xLightPosition, inS.Position3D, inS.Normal);
			else if(xLightType == 2)
				diffuseColorFactor = dot(inS.Normal, -xLightDirection);
			diffuseColorFactor = saturate(diffuseColorFactor);
			diffuseColorFactor *= xLightPower;
			diffuseColorFactor += xAmbient;
		}
	}
		
	float depthDistance = 0.99f;
	float depthWidth = 0.005f;
	float blendFactor = clamp((inS.Depth-depthDistance)/depthWidth, 0.0f, 1.0f);
	
	float4 farColor;
	farColor = tex2D(TextureSampler0, inS.TextureCoords) * inS.TextureWeights.x;
	farColor += tex2D(TextureSampler1, inS.TextureCoords) * inS.TextureWeights.y;
	farColor += tex2D(TextureSampler2, inS.TextureCoords) * inS.TextureWeights.z;
	farColor += tex2D(TextureSampler3, inS.TextureCoords) * inS.TextureWeights.w;
	
	float4 nearColor;
	float2 textureCoords = inS.TextureCoords * 3.0f;
	nearColor = tex2D(TextureSampler0, textureCoords) * inS.TextureWeights.x;
	nearColor += tex2D(TextureSampler1, textureCoords) * inS.TextureWeights.y;
	nearColor += tex2D(TextureSampler2, textureCoords) * inS.TextureWeights.z;
	nearColor += tex2D(TextureSampler3, textureCoords) * inS.TextureWeights.w;
	
	Output.Color = lerp(nearColor, farColor, blendFactor);
	Output.Color *= diffuseColorFactor;
	Output.Color.a = 1.0;
	
	return Output;
}

technique ShadowedSceneMultiTextured
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 ShadowedSceneMultiTexturedVS();
		PixelShader = compile ps_2_0 ShadowedSceneMultiTexturedPS();
	}
}


// -- Water -- //
float4x4 xReflectionView;
Texture xReflectionMap;
Texture xRefractionMap;
Texture xWaterBumpMap;
float xWaveLength;
float xWaveHeight;
float3 xCamPos;
float xTime;
float3 xWindDirection;
float xWindForce;

sampler ReflectionMapSampler = sampler_state { texture = <xReflectionMap>; minfilter=LINEAR; magfilter=LINEAR; mipfilter=LINEAR;
	AddressU=mirror; AddressV=mirror;};
sampler RefractionMapSampler = sampler_state { texture = <xRefractionMap>; minfilter=LINEAR; magfilter=LINEAR; mipfilter=LINEAR;
	AddressU=mirror; AddressV=mirror;};
sampler WaterBumpMapSampler = sampler_state { texture=<xWaterBumpMap>; minfilter=LINEAR; magfilter=LINEAR; mipfilter=LINEAR;
	AddressU=mirror; AddressV=mirror;};
	
struct WaterVertexToPixel
{
	float4 Position		: POSITION;
	float4 ReflectionMapSamplingPos	: TEXCOORD1;
	float2 BumpMapSamplingPos	: TEXCOORD2;
	float4 RefractionMapSamplingPos : TEXCOORD3;
	float4 Position3D	: TEXCOORD4;
};

struct WaterPixelToFrame
{
	float4 Color	: COLOR0;
};

WaterVertexToPixel WaterVertexShader(float4 inPos:POSITION, float2 inTextureCoords:TEXCOORD0)
{
	WaterVertexToPixel Output = (WaterVertexToPixel)0;
	
	float4x4 preViewProjection = mul(xView, xProjection);
	float4x4 preWorldViewProjection = mul(xWorld, preViewProjection);
	Output.Position = mul(inPos, preWorldViewProjection);
	Output.Position3D = mul(inPos, xWorld);
	
	float4x4 preReflectionViewProjection = mul(xReflectionView, xProjection);
	float4x4 preReflectionWorldViewProjection = mul(xWorld, preReflectionViewProjection);
	Output.ReflectionMapSamplingPos = mul(inPos, preReflectionWorldViewProjection);
	
	float3 windDir = normalize(xWindDirection);				//nomalized
	float3 perpDirR = cross(xWindDirection, float3(0,1,0));	//non-nomalized (just to obtain the projection on the line)
	
	float yDot = dot(inTextureCoords, windDir.xz);
	float xDot = dot(inTextureCoords, perpDirR.xz);
	float2 moveDir = float2(xDot, yDot);
	moveDir.y += xWindForce * xTime;
	Output.BumpMapSamplingPos = moveDir/xWaveLength;

	Output.RefractionMapSamplingPos = mul(inPos, preWorldViewProjection);
	
	return Output;
}

WaterPixelToFrame WaterPixelShader(WaterVertexToPixel inS)
{
	WaterPixelToFrame Output = (WaterPixelToFrame)0;
	
	float2 projectedTexCoords;
	
	projectedTexCoords.x = inS.ReflectionMapSamplingPos.x/inS.ReflectionMapSamplingPos.w / 2.0f + 0.5f;
	projectedTexCoords.y = -inS.ReflectionMapSamplingPos.y/inS.ReflectionMapSamplingPos.w / 2.0f + 0.5f;
	
	float4 bumpColor;
	bumpColor = tex2D(WaterBumpMapSampler, inS.BumpMapSamplingPos);	//in range [0,1]
	float2 perturbation = xWaveHeight*(bumpColor.rg - 0.5f) * 2.0f;	//in range [-1,1]
	float2 perturbatedTexCoords = perturbation + projectedTexCoords;

	//get the reflect color
	float4 reflectionColor = tex2D(ReflectionMapSampler, perturbatedTexCoords);
	
	projectedTexCoords.x = inS.RefractionMapSamplingPos.x/inS.RefractionMapSamplingPos.w / 2.0f + 0.5f;
	projectedTexCoords.y = -inS.RefractionMapSamplingPos.y/inS.RefractionMapSamplingPos.w / 2.0f + 0.5f;
	float2 perturbatedRefractionCoords = perturbation + projectedTexCoords;
	//get the refract color
	float4 refractionColor = tex2D(RefractionMapSampler, perturbatedRefractionCoords);
	
	//blend them together
	float3 eyeVector = normalize(xCamPos - inS.Position3D);
	float xCom = (bumpColor.r-0.5f)*0.2f;
	xCom = lerp(xCom, 1.0f, 0.54f);
	float yCom = (bumpColor.g-0.5f)*0.2f;
	yCom = lerp(yCom, 1.0f, 0.43f);
	float zCom = (bumpColor.b-0.5f)*0.2f;
	zCom = lerp(zCom, 1.0f, 0.78f);
	float3 normalW = float3(xCom, yCom, zCom);
	float fresnelTerm = dot(eyeVector, normalW);

	float4 combinedColor = lerp(refractionColor, reflectionColor, fresnelTerm);
	
	//blend with dull color
	float4 dullColor = float4(0.2f, 0.2f, 0.4f, 1.0f);
	
	Output.Color = lerp(combinedColor, dullColor, 0.20f);
	
	//specular highlight
	float3 reflect = -reflect(xLightDirection, normalW);
	float specular = dot(normalize(reflect), normalize(eyeVector));
	
	specular = pow(specular, 256);
	Output.Color.rgb += specular;
	
	return Output;
}

technique Water
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 WaterVertexShader();
		PixelShader = compile ps_2_0 WaterPixelShader();
	}
}

// -- Perlin Technique -- //
Texture xStaticMap;
sampler StaticMapSampler = sampler_state { texture=<xStaticMap>; minfilter=LINEAR; magfilter=LINEAR; mipfilter=LINEAR;
	AddressU=mirror; AddressV=mirror;};
float xOvercast;

struct PNVertexToPixel
{
	float4 Position		: POSITION;
	float2 TextureCoords: TEXCOORD0;
};

struct PNPixelToFrame
{
	float4 Color	: COLOR0;
};

PNVertexToPixel PNVertexShader(float4 inPos:POSITION, float2 inTexCoords:TEXCOORD0)
{
	PNVertexToPixel Output = (PNVertexToPixel)0;
	
	Output.Position = inPos;
	Output.TextureCoords = inTexCoords;
	
	return Output;
}

PNPixelToFrame PNPixelShader(PNVertexToPixel inS)
{
	PNPixelToFrame Output = (PNPixelToFrame)0;
	
	float2 move = float2(0,1);
	float4 perlin = tex2D(StaticMapSampler, inS.TextureCoords+xTime*move)/2.0f;
	perlin += tex2D(StaticMapSampler, inS.TextureCoords*2.0f+xTime*move)/4.0f;
	perlin += tex2D(StaticMapSampler, inS.TextureCoords*4.0f+xTime*move)/8.0f;
	perlin += tex2D(StaticMapSampler, inS.TextureCoords*8.0f+xTime*move)/16.0f;
	perlin += tex2D(StaticMapSampler, inS.TextureCoords*16.0f+xTime*move)/32.0f;
	perlin += tex2D(StaticMapSampler, inS.TextureCoords*32.0f+xTime*move)/32.0f;
	
	Output.Color = 1.0f - pow(perlin, xOvercast)*2.0f;
	
	return Output;
}

technique PerlinNoise
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 PNVertexShader();
		PixelShader = compile ps_2_0 PNPixelShader();
	}
}

// -- Perlin Static Technique -- //

struct PNSVertexToPixel
{
	float4 Position		: POSITION;
	float2 TextureCoords: TEXCOORD0;
};

struct PNSPixelToFrame
{
	float4 Color	: COLOR0;
};

PNSVertexToPixel PNSVertexShader(float4 inPos:POSITION, float2 inTexCoords:TEXCOORD0)
{
	PNVertexToPixel Output = (PNVertexToPixel)0;
	
	Output.Position = inPos;
	Output.TextureCoords = inTexCoords;
	
	return Output;
}

PNSPixelToFrame PNSPixelShader(PNVertexToPixel inS)
{
	PNPixelToFrame Output = (PNPixelToFrame)0;
	
	float2 move = float2(0,1);
	float4 perlin = tex2D(StaticMapSampler, inS.TextureCoords)/2.0f;
	perlin += tex2D(StaticMapSampler, inS.TextureCoords*2.0f)/4.0f;
	perlin += tex2D(StaticMapSampler, inS.TextureCoords*4.0f)/8.0f;
	perlin += tex2D(StaticMapSampler, inS.TextureCoords*8.0f)/16.0f;
	perlin += tex2D(StaticMapSampler, inS.TextureCoords*16.0f)/32.0f;
	perlin += tex2D(StaticMapSampler, inS.TextureCoords*32.0f)/32.0f;
	
	Output.Color = 1.0f - pow(perlin, xOvercast)*2.0f;
	
	return Output;
}

technique PerlinNoiseStatic
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 PNSVertexShader();
		PixelShader = compile ps_2_0 PNSPixelShader();
	}
}

// -- SkyDome -- //
float4 xSkyDomeTopColor;
float4 xSkyDomeBaseColor;

struct SDVertexToPixel
{
	float4 Position	: POSITION;
	float2 TextureCoords	: TEXCOORD0;
	float4 ObjectPosition	: TEXCOORD1;
};

struct SDPixelToFrame
{
	float4 Color	: COLOR0;
};

SDVertexToPixel SDVertexShader(float4 inPos:POSITION, float2 inTex:TEXCOORD0)
{
	SDVertexToPixel Output = (SDVertexToPixel)0;
	
	float4x4 preViewProjection = mul(xView, xProjection);
	float4x4 preWorldViewProjection = mul(xWorld, preViewProjection);
	
	Output.Position = mul(inPos, preWorldViewProjection);
	Output.TextureCoords = inTex;
	Output.ObjectPosition = inPos;
	
	return Output;
}

SDPixelToFrame SDPixelShader(SDVertexToPixel inS)
{
	SDPixelToFrame Output = (SDPixelToFrame)0;
	
	float4 baseColor = lerp(xSkyDomeBaseColor, xSkyDomeTopColor, saturate((inS.ObjectPosition.y/inS.ObjectPosition.w)/0.4f));
	float4 cloudColor = tex2D(TextureSampler, inS.TextureCoords).r;
	
	Output.Color = lerp(baseColor, 1, cloudColor);
	
	return Output;
}

technique SkyDome
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 SDVertexShader();
		PixelShader = compile ps_2_0 SDPixelShader();
	}
}

float xModelLightLevelPower;

// -- ModelColored -- //
struct ModelColoredVertexToPixel
{
	float4 Position		: POSITION;
	float4 Color		: COLOR;
	float3 Normal		: TEXCOORD0;
	float3 Position3D	: TEXCOORD1;
};

struct ModelColoredPixelToFrame
{
	float4 Color		: COLOR0;
};

ModelColoredVertexToPixel ModelColoredVertexShader(float4 inPos:POSITION, float4 inColor:COLOR0, float3 inNormal:NORMAL)
{
	ModelColoredVertexToPixel Output = (ModelColoredVertexToPixel)0;
	
	float4x4 preViewProjection = mul(xView, xProjection);
	float4x4 preWorldViewProjection = mul(xWorld, preViewProjection);
	
	Output.Position = mul(inPos, preWorldViewProjection);
	Output.Color = inColor;
	Output.Normal = normalize(mul(inNormal, (float3x3)xWorld));
	Output.Position3D = mul(inPos, xWorld);
	
	return Output;
}

ModelColoredPixelToFrame ModelColoredPixelShader(ModelColoredVertexToPixel PSIn)
{
	ModelColoredPixelToFrame Output = (ModelColoredPixelToFrame)0;
	
	float diffuseLightFactor = 1.0f;
	if(xLightEnabled){
		if(xLightType == 1)
			diffuseLightFactor = DotProduct(xLightPosition, PSIn.Position3D, PSIn.Normal);
		else if(xLightType == 2)
			diffuseLightFactor = saturate(dot(PSIn.Normal, -xLightDirection));
	}
	
	if(xSolidBrown)
		Output.Color = float4(0.25f, 0.21f, 0.20f, 1);
	else
		Output.Color = PSIn.Color;

	//tint it with the light's ambient color
	float4 lightAmbientColor = float4(xLightAmbientColor.r, xLightAmbientColor.g, xLightAmbientColor.b, 1.0f);
	//Output.Color = (Output.Color + lightAmbientColor * xLightAmbientColorFactor) * diffuseLightFactor * xModelLightLevelPower;
	Output.Color = (Output.Color + (lightAmbientColor * xLightAmbientColorFactor + xAmbient)) * diffuseLightFactor * xModelLightLevelPower * xLightPower;
	
	return Output;
}

technique ModelColored
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 ModelColoredVertexShader();
		PixelShader = compile ps_2_0 ModelColoredPixelShader();
	}
}

// -- ModelTextured-- //
struct ModelTexturedVertexToPixel
{
	float4 Position		: POSITION;
	float2 TexCoord		: TEXCOORD0;
	float3 Normal		: TEXCOORD1;
	float3 Position3D	: TEXCOORD2;
};

struct ModelTexturedPixelToFrame
{
	float4 Color		: COLOR0;
};

ModelTexturedVertexToPixel ModelTexturedVertexShader(float4 inPos:POSITION, float2 inTexCoord:TEXCOORD0, float3 inNormal:NORMAL)
{
	ModelTexturedVertexToPixel Output = (ModelTexturedVertexToPixel)0;
	
	float4x4 preViewProjection = mul(xView, xProjection);
	float4x4 preWorldViewProjection = mul(xWorld, preViewProjection);
	
	Output.Position = mul(inPos, preWorldViewProjection);
	Output.TexCoord = inTexCoord;
	Output.Normal = normalize(mul(inNormal, (float3x3)xWorld));
	Output.Position3D = mul(inPos, xWorld);
	
	return Output;
}

ModelTexturedPixelToFrame ModelTexturedPixelShader(ModelTexturedVertexToPixel PSIn)
{
	ModelTexturedPixelToFrame Output = (ModelTexturedPixelToFrame)0;
	
	float diffuseLightFactor = 1.0f;
	if(xLightEnabled){
		if(xLightType == 1)
			diffuseLightFactor = DotProduct(xLightPosition, PSIn.Position3D, PSIn.Normal);
		else if(xLightType == 2)
			diffuseLightFactor = saturate(dot(PSIn.Normal, -xLightDirection));
	}
	
	if(xSolidBrown)
		Output.Color = float4(0.25f, 0.21f, 0.20f, 1);
	else
		Output.Color = tex2D(TextureSampler, PSIn.TexCoord);

	//diffuse light color already saturated
	//tint it with the light's ambient color
	float4 lightAmbientColor = float4(xLightAmbientColor.r, xLightAmbientColor.g, xLightAmbientColor.b, 1.0f);
	Output.Color = (Output.Color + (lightAmbientColor * xLightAmbientColorFactor + xAmbient)) * diffuseLightFactor * xModelLightLevelPower * xLightPower;
	
	return Output;
}

technique ModelTextured
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 ModelTexturedVertexShader();
		PixelShader = compile ps_2_0 ModelTexturedPixelShader();
	}
}

//-- Technique: ShadowMap -- //
struct ShadowMapToPixel
{
	float4 Position	: POSITION;
	float4 DiffPositionFromLightSource : TEXCOORD0;
};

struct ShadowMapToFrame
{
	float4 Color : COLOR;
};

ShadowMapToPixel ShadowMapVS(float4 inPos:POSITION)
{
	ShadowMapToPixel Output = (ShadowMapToPixel)0;
	
	float4x4 preLightViewProjection = mul(xView, xProjection);
	float4x4 preLightWorldViewProjection = mul(xWorld, preLightViewProjection);
	
	Output.Position = mul(inPos, preLightWorldViewProjection);
	Output.DiffPositionFromLightSource = Output.Position;
	
	return Output;
}

ShadowMapToFrame ShadowMapPS(ShadowMapToPixel inS)
{
	ShadowMapToFrame Output = (ShadowMapToFrame)0;
	
	Output.Color = inS.DiffPositionFromLightSource.z / inS.DiffPositionFromLightSource.w;
	
	return Output;
}

technique ShadowMap
{
	pass Pass0
    {   
    	VertexShader = compile vs_2_0 ShadowMapVS();
        PixelShader  = compile ps_2_0 ShadowMapPS();
    }
}

//-- Technique: ShadowedScene --//
//Note: Use xShadowMap, xTexture as object's tecture, xLightView, xLightProjection, xBiasedDistance

struct ShadowedSceneToPixel
{
	float4 Position	: POSITION;
	float3 Normal : TEXCOORD0;
	float2 TextureCoords : TEXCOORD1;
	float4 Position2D_SeenByLight : TEXCOORD2;
	float4 Position3D : TEXCOORD3;
};

struct ShadowedSceneToFrame
{
	float4 Color : COLOR0;
};

ShadowedSceneToPixel ShadowedSceneVS(float4 inPos:POSITION, float2 inTexCoords:TEXCOORD0, float3 inNormal:NORMAL)
{
	ShadowedSceneToPixel Output = (ShadowedSceneToPixel)0;
	
	//camera
	float4x4 preViewProjection = mul(xView, xProjection);
	float4x4 preWorldViewProjection = mul(xWorld, preViewProjection);
	
	//light
	float4x4 preLightViewProjection = mul(xLightView, xLightProjection);
	float4x4 preLightWorldViewProjection = mul(xWorld, preLightViewProjection);
	
	Output.Position = mul(inPos, preWorldViewProjection);
	Output.Normal = normalize(mul(inNormal, (float3x3)xWorld));
	Output.TextureCoords = inTexCoords;
	Output.Position3D = mul(inPos, xWorld);
	Output.Position2D_SeenByLight = mul(inPos, preLightWorldViewProjection);
	
	return Output;
}

ShadowedSceneToFrame ShadowedScenePS(ShadowedSceneToPixel inS)
{
	ShadowedSceneToFrame Output = (ShadowedSceneToFrame)0;
	
	//calculate the texture coordinate for the shadow map
	//Note: We must convert from [-1,1] to [0,1] due to we have passed the interpolation process.
	float2 projectedTexCoords;
	projectedTexCoords.x = inS.Position2D_SeenByLight.x / inS.Position2D_SeenByLight.w / 2.0f + 0.5f;
	projectedTexCoords.y = -inS.Position2D_SeenByLight.y / inS.Position2D_SeenByLight.w / 2.0f + 0.5f;
	
	//checking if the coordinate is in range of the view frustum
	float diffuseColorFactor = 0.0f;
	if(xEnabledShadowedSceneCustomInitDiffuseColorFactor)
		diffuseColorFactor = xBaseDiffuseColorFactor;
	if(saturate(projectedTexCoords.x) == projectedTexCoords.x && saturate(projectedTexCoords.y) == projectedTexCoords.y && xLightEnabled)
	{
		float depthStoredInMap = tex2D(ShadowMapSampler, projectedTexCoords).r;
		float realDistance = inS.Position2D_SeenByLight.z / inS.Position2D_SeenByLight.w;
		if(realDistance - 1.0f / xBiasedDistance <= depthStoredInMap)
		{
			//calculate the diffuse color factor
			if(xLightType == 1)
				diffuseColorFactor = DotProduct(xLightPosition, inS.Position3D, inS.Normal);
			else if(xLightType == 2)
				diffuseColorFactor = dot(inS.Normal, -xLightDirection);
			
			diffuseColorFactor = saturate(diffuseColorFactor);
			diffuseColorFactor *= xLightPower;
			diffuseColorFactor += xAmbient;
		}
	}
	//blend it with the object's texture color
	float4 baseColor = tex2D(TextureSampler, inS.TextureCoords);
	//tint it with the light's ambient color
	float4 lightAmbientColor = float4(xLightAmbientColor.r, xLightAmbientColor.g, xLightAmbientColor.b, 1.0f);
	Output.Color = baseColor * (lightAmbientColor + xLightAmbientColorFactor) * diffuseColorFactor;
	Output.Color.a = 1.0f;
	
	return Output;
}

technique ShadowedScene
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 ShadowedSceneVS();
		PixelShader = compile ps_2_0 ShadowedScenePS();
	}
} 
