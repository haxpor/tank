// -- Line -- //
float4x4 xView;
float4x4 xProjection;
float4x4 xWorld;

struct LineVertexToPixel
{
	float4 Position: POSITION;
	float4 Color : COLOR;
};

struct LinePixelToFrame
{
	float4 Color : COLOR;
};

LineVertexToPixel LineVertexShader(float4 inPos:POSITION, float4 inColor:COLOR)
{
	LineVertexToPixel Output = (LineVertexToPixel)0;
	
	float4x4 preViewProjection = mul(xView, xProjection);
	float4x4 preWorldViewProjection = mul(xWorld, preViewProjection);
	
	Output.Position = mul(inPos, preWorldViewProjection);
	Output.Color = inColor;
	
	return Output;
}

LinePixelToFrame LinePixelShader(LineVertexToPixel inS)
{
	LinePixelToFrame Output = (LinePixelToFrame)0;
	
	Output.Color = inS.Color;
	
	return Output;
}

technique Line
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 LineVertexShader();
		PixelShader = compile ps_2_0 LinePixelShader();
	}
}