﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace TankMovementAnimation
{
    class Tank
    {
        Model tank;

        //All the bones of tank model
        ModelBone tank_right_back_wheel_bone;
        ModelBone tank_left_back_wheel_bone;
        ModelBone tank_right_steer_bone;
        ModelBone tank_left_steer_bone;
        ModelBone tank_left_front_wheel_bone;
        ModelBone tank_right_front_wheel_bone;
        ModelBone tank_hatch_bone;
        ModelBone tank_cannon_bone;
        ModelBone tank_turret_bone;
        ModelBone tank_bone;

        //The transformation holder
        Matrix[] tank_transforms;

        //Original transforms of all the tank model's bones
        Matrix tank_right_back_wheel_transform;
        Matrix tank_left_back_wheel_transform;
        Matrix tank_right_steer_transform;
        Matrix tank_left_steer_transform;
        Matrix tank_left_front_wheel_transform;
        Matrix tank_right_front_wheel_transform;
        Matrix tank_hatch_transform;
        Matrix tank_cannon_transform;
        Matrix tank_turret_transform;
        Matrix tank_transform;

        //Param value used in transform
        float leftRightBackWheelRotationValue;
        float leftRightFrontWheelRotationValue;
        float leftRightSteerRotationValue;
        float hatchRotationValue;
        float cannonRotationValue;
        float turretRotationValue;
        float leftRightTankRotationValue;       //for terrain surface
        float upDownTankRotationValue;          //for terrain surface
        float leftRightTankRotationY_RealisticValue;      //for realistic maneuver

        //Tank world matrix
        Matrix world;

        //Position
        Vector3 position = new Vector3(0, 0, 0);

        //Coordinate system
        Vector3 lookforward = new Vector3(0, 0, -1);
        Vector3 up = new Vector3(0, 1, 0);
        Vector3 cannonForward = new Vector3(0, 0, -1);
        Vector3 cannonBulletDir = new Vector3(0, 0, -1);

        //Textures attached to model
        Texture2D[] textures;

        //Realistic Movement vectors
        Vector3 centerLeft = new Vector3(-1, 0, 0);
        Vector3 centerRight = new Vector3(1, 0, 0);
        Vector3 middleFront = new Vector3(0, 0, -1);
        Vector3 middleBack = new Vector3(0, 0, 1);

        //two different of forward direction that camera should obey
        Matrix tankRotation;
        Matrix cannonRotation;
        Matrix cannonBulletDirRotation;

        //temp quaternion used for internal calculation
        Quaternion rotTemp = Quaternion.Identity;

        //Misc Settings
        public LightSettings LightSettings;
        public EffectSettings EffectSettings;

        public Matrix TankRotation
        {
            get { return tankRotation; }
        }

        public Matrix CannonRotation
        {
            get { return cannonRotation; }
        }

        public Matrix CannonBulletDirRotation
        {
            get { return cannonBulletDirRotation; }
        }

        public Model Model
        {
            get { return tank; }
        }

        public Vector3 CannonForwardVector
        {
            get { return cannonForward; }
        }

        public Vector3 CannonBulletDirVector
        {
            get { return cannonBulletDir; }
        }

        public Vector3 LookForwardVector
        {
            get { return lookforward; }
        }

        public Vector3 UpVector
        {
            get { return up; }
        }

        public Vector3 Position
        {
            get { return position; }
            set
            {
                position += value;
            }
        }

        public float Height
        {
            get { return position.Y; }
            set
            {
                position.Y = value;
            }
        }

        public Matrix WorldMatrix
        {
            get { return world; }
        }

        public float BackWheelRotationAngle
        {
            get { return leftRightBackWheelRotationValue; }
            set
            {
                leftRightBackWheelRotationValue += value;
                if (leftRightBackWheelRotationValue > MathHelper.TwoPi || leftRightBackWheelRotationValue < -MathHelper.TwoPi)
                    leftRightBackWheelRotationValue = 0.0f;
            }
        }

        public float FrontWheelRotationAngle
        {
            get { return leftRightFrontWheelRotationValue; }
            set
            {
                leftRightFrontWheelRotationValue += value;
                if (leftRightFrontWheelRotationValue > MathHelper.TwoPi || leftRightFrontWheelRotationValue < -MathHelper.TwoPi)
                    leftRightFrontWheelRotationValue = 0.0f;
            }
        }

        public float HatchRotationAngle
        {
            get { return hatchRotationValue; }
            set
            {
                hatchRotationValue += value;
                if (hatchRotationValue < -MathHelper.PiOver2)
                    hatchRotationValue = -MathHelper.PiOver2;
                if (hatchRotationValue > 0.0f)
                    hatchRotationValue = 0.0f;
            }
        }

        public float SteerRotationAngle
        {
            get { return leftRightSteerRotationValue; }
            set
            {
                //back to normal direction
                if (value == 0.0f)
                {
                    if (leftRightSteerRotationValue < 0.0f)
                        leftRightSteerRotationValue += 0.03f;
                    else if (leftRightSteerRotationValue > 0.0f)
                        leftRightSteerRotationValue -= 0.03f;
                }
                else
                {
                    leftRightSteerRotationValue += value;
                    if (leftRightSteerRotationValue > MathHelper.PiOver4)
                    {
                        leftRightSteerRotationValue = MathHelper.PiOver4;
                    }
                    if (leftRightSteerRotationValue < -MathHelper.PiOver4)
                    {
                        leftRightSteerRotationValue = -MathHelper.PiOver4;
                    }
                }
            }
        }

        public float RealisticManeuverAngle
        {
            get { return leftRightTankRotationY_RealisticValue; }
            set
            {
                leftRightTankRotationY_RealisticValue += value;
                if (leftRightTankRotationY_RealisticValue > MathHelper.TwoPi || leftRightTankRotationY_RealisticValue < -MathHelper.TwoPi)
                    leftRightTankRotationY_RealisticValue = 0.0f;
            }
        }

        public float CannonRotationAngle
        {
            get { return cannonRotationValue; }
            set
            {
                cannonRotationValue += value;
                if (cannonRotationValue < -MathHelper.PiOver2)
                    cannonRotationValue = -MathHelper.PiOver2;
                if (cannonRotationValue > 0.0f)
                    cannonRotationValue = 0.0f;
            }
        }

        public float TurretRotationAngle
        {
            get { return turretRotationValue; }
            set
            {
                turretRotationValue += value;
                if (turretRotationValue > MathHelper.TwoPi || turretRotationValue < -MathHelper.TwoPi)
                    turretRotationValue = 0.0f;
            }
        }

        //Constructor
        public Tank(string filename, ContentManager Content, GraphicsDevice device, bool withBoundingSphere)
        {
            if (withBoundingSphere)
                XNAUtil.LoadModelWithBoundingSphere(device, Content, filename, out tank);
            else
                XNAUtil.LoadModel(Content, filename, out tank);
            Load(Content);
        }

        //Constructor
        //Load the model with the basic effect
        public Tank(string filename, ContentManager Content, GraphicsDevice device, ref BasicEffect basicEffect, bool withTextures, bool withBoundingSphere)
        {
            if (withBoundingSphere && withTextures)
                XNAUtil.LoadModelWithBoundingSphere(device, Content, filename, ref basicEffect, out textures, out tank);
            else if (withBoundingSphere && !withTextures)
                XNAUtil.LoadModel(device, Content, filename, ref basicEffect, out tank);
            else
                XNAUtil.LoadModel(Content, filename, out tank);

            Load(Content);
        }

        //Constructor
        public Tank(string filename, ContentManager Content, GraphicsDevice device, ref Effect effect, bool withTextures, bool withBoundingSphere)
        {
            if (withBoundingSphere && withTextures)
                XNAUtil.LoadModelWithBoundingSphere(device, Content, filename, ref effect, out textures, out tank);
            else if (withBoundingSphere && !withTextures)
                XNAUtil.LoadModelWithBoundingSphere(device, Content, filename, ref effect, out tank);
            else
                XNAUtil.LoadModel(Content, filename, out tank);
            Load(Content);
        }

        //Constructor (with the preload model)
        public Tank(ref Model model, ContentManager Content, GraphicsDevice device, bool withBoundingSphere)
        {
            //Set our model to the given model
            tank = model;

            if (withBoundingSphere)
                XNAUtil.LoadModelWithBoundingSphere(device, Content, ref tank);

            Load(Content);
        }

        //Constructor (with the preload model)
        //with basic effect
        public Tank(ref Model model, ContentManager Content, GraphicsDevice device, ref BasicEffect basicEffect, bool withTextures, bool withBoundingSphere)
        {
            //Set our model to the given model
            tank = model;

            if (withBoundingSphere && withTextures)
                XNAUtil.LoadModelWithBoundingSphere(device, Content, ref tank, ref basicEffect, out textures);
            else if (withBoundingSphere && !withTextures)
                XNAUtil.LoadModelWithBoundingSphere(device, Content, ref tank, ref basicEffect);

            Load(Content);
        }

        //Constructor (with the preload model)
        public Tank(ref Model model, ContentManager Content, GraphicsDevice device, ref Effect effect, bool withTextures, bool withBoundingSphere)
        {
            //Set our model to the given model
            tank = model;

            if (withBoundingSphere && withTextures)
                XNAUtil.LoadModelWithBoundingSphere(device, Content, ref tank, ref effect, out textures);
            else if (withBoundingSphere && !withTextures)
                XNAUtil.LoadModelWithBoundingSphere(device, Content, ref tank, ref effect);

            Load(Content);
        }

        // Move the tank forward
        public void MoveForward(float relativeAmount)
        {
            //rotate the lookforward vector is already performed in Update() method
            //now move forward
            position += lookforward * relativeAmount;
        }

        // Move the tank backward
        public void MoveBackward(float relativeAmount)
        {
            //rotate the lookforward vector is already performed in Update() method
            //now move backward
            position -= lookforward * relativeAmount;
        }

        private void Load(ContentManager Content)
        {
            //Create the lightsettings
            LightSettings = new LightSettings();
            EffectSettings = new EffectSettings();

            //get the bones from model
            tank_right_back_wheel_bone = tank.Bones["r_back_wheel_geo"];
            tank_left_back_wheel_bone = tank.Bones["l_back_wheel_geo"];
            tank_right_steer_bone = tank.Bones["r_steer_geo"];
            tank_left_steer_bone = tank.Bones["l_steer_geo"];
            tank_left_front_wheel_bone = tank.Bones["l_front_wheel_geo"];
            tank_right_front_wheel_bone = tank.Bones["r_front_wheel_geo"];
            tank_hatch_bone = tank.Bones["hatch_geo"];
            tank_cannon_bone = tank.Bones["canon_geo"];
            tank_turret_bone = tank.Bones["turret_geo"];
            tank_bone = tank.Bones["tank_geo"];

            //store the original transform matrix for each bones
            tank_right_back_wheel_transform = tank_right_back_wheel_bone.Transform;
            tank_left_back_wheel_transform = tank_left_back_wheel_bone.Transform;
            tank_right_steer_transform = tank_right_steer_bone.Transform;
            tank_left_steer_transform = tank_left_steer_bone.Transform;
            tank_left_front_wheel_transform = tank_left_front_wheel_bone.Transform;
            tank_right_front_wheel_transform = tank_right_front_wheel_bone.Transform;
            tank_hatch_transform = tank_hatch_bone.Transform;
            tank_cannon_transform = tank_cannon_bone.Transform;
            tank_turret_transform = tank_turret_bone.Transform;
            tank_transform = tank_bone.Transform;

            //allocate the space for the transforms holder
            tank_transforms = new Matrix[tank.Bones.Count];
        }

        // Realistic terrain surface calculation
        public void CalculateRealisticMovementOnTerrainSurface(ref Terrain terrain, Matrix world)
        {
            //get bounding sphere and scale it with the current world matrix
            BoundingSphere boundingSphere = (BoundingSphere)tank.Tag;
            BoundingSphere scaledBoundingSphere = XNAUtil.TransformBoundingSphere(boundingSphere, world);

            //get the radius of boundingsphere
            float radius = scaledBoundingSphere.Radius;
            float radiusMul2 = scaledBoundingSphere.Radius * 2.0f;

            //transfrom the realistic vectors
            Matrix upDownTankRotation = Matrix.CreateRotationX(upDownTankRotationValue);
            Matrix leftRightTankRotation = Matrix.CreateRotationZ(leftRightTankRotationValue);
            Matrix leftRightTankRotationY_RealisticRotation = Matrix.CreateRotationY(leftRightTankRotationY_RealisticValue);

            // use quaternion for safe way to calculating the angle rotation
            rotTemp = Quaternion.CreateFromRotationMatrix(upDownTankRotation)
                * Quaternion.CreateFromRotationMatrix(leftRightTankRotation)
                * Quaternion.CreateFromRotationMatrix(leftRightTankRotationY_RealisticRotation);
            //Matrix tankRotation = upDownTankRotation * leftRightTankRotationY_RealisticRotation * leftRightTankRotation;
            Matrix tankRotation = Matrix.CreateFromQuaternion(rotTemp);

            Vector3 transformed_centerLeft = Vector3.Transform(centerLeft, tankRotation);
            Vector3 transformed_centerRight = Vector3.Transform(centerRight, tankRotation);
            Vector3 transformed_middleFront = Vector3.Transform(middleFront, tankRotation);
            Vector3 transformed_middleBack = Vector3.Transform(middleBack, tankRotation);

            //calculate the up vector (for more accuracy)
            Vector3 cross = transformed_centerRight - transformed_centerLeft;
            Vector3 vertical = transformed_middleFront - transformed_middleBack;
            up = Vector3.Cross(cross, vertical);
            up = Vector3.Normalize(up);

            //scale them
            transformed_centerLeft *= radius;
            transformed_centerRight *= radius;
            transformed_middleBack *= radius;
            transformed_middleFront *= radius;

            //adjust the realistic vectors
            Vector3 adjustedTransformed_centerLeft = transformed_centerLeft + position;
            Vector3 adjustedTransformed_centerRight = transformed_centerRight + position;
            Vector3 adjustedTransformed_middleFront = transformed_middleFront + position;
            Vector3 adjustedTransformed_middleBack = transformed_middleBack + position;

            //get height of all the realistic vectors reside
            float y_centerLeft = terrain.GetHeight(adjustedTransformed_centerLeft);
            float y_centerRight = terrain.GetHeight(adjustedTransformed_centerRight);
            float y_middleFront = terrain.GetHeight(adjustedTransformed_middleFront);
            float y_middleBack = terrain.GetHeight(adjustedTransformed_middleBack);

            //calcualte the angle to rotate the tank model
            float horizontalAngle_z = (float)Math.Atan2(y_centerLeft - y_centerRight, radiusMul2);
            float verticalAngle_x = (float)Math.Atan2(y_middleBack - y_middleFront, radiusMul2);

            //set it globally
            leftRightTankRotationValue = horizontalAngle_z;
            upDownTankRotationValue = verticalAngle_x;

            //apply transform
            //** this will be performed in Update() method
        }

        public void Update(GameTime gameTime, Matrix world)
        {
            //save the current world matrix of the tank
            this.world = world * Matrix.CreateTranslation(position);
            //set the world matrix to the root of tank model
            //tank.Root.Transform = this.world;

            Matrix leftRightBackWheelRotation = Matrix.CreateRotationX(leftRightBackWheelRotationValue);
            Matrix leftRightFrontWheelRotation = Matrix.CreateRotationX(leftRightFrontWheelRotationValue);
            Matrix leftRightSteerRotation = Matrix.CreateRotationY(leftRightSteerRotationValue);
            Matrix hatchRotation = Matrix.CreateRotationX(hatchRotationValue);
            Matrix cannonRotation = Matrix.CreateRotationX(cannonRotationValue);
            Matrix turretRotation = Matrix.CreateRotationY(turretRotationValue);
            Matrix leftRightTankRotation = Matrix.CreateRotationZ(leftRightTankRotationValue);
            Matrix upDownTankRotation = Matrix.CreateRotationX(upDownTankRotationValue);
            Matrix leftRightTankRotationY_RealisticRotation = Matrix.CreateRotationY(leftRightTankRotationY_RealisticValue);

            // use quaternion for safe way to calculate the angle rotation
            rotTemp = Quaternion.CreateFromRotationMatrix(Matrix.Invert(upDownTankRotation))
                * Quaternion.CreateFromRotationMatrix(leftRightTankRotationY_RealisticRotation)
                * Quaternion.CreateFromRotationMatrix(leftRightTankRotation);

            //Invert the upDownTankRotation due to we use middleBack to subtract middleFront
            Matrix tankRotation = Matrix.CreateFromQuaternion(rotTemp);
            Matrix global_cannonRotation = tankRotation * turretRotation;

            // use quaternion for safe way to calculate the angle rotation
            rotTemp = Quaternion.CreateFromRotationMatrix(global_cannonRotation)
                * Quaternion.CreateFromRotationMatrix(Matrix.Invert(cannonRotation));
            Matrix cannonBulletRotation = Matrix.CreateFromQuaternion(rotTemp);

            //set it globally
            this.tankRotation = tankRotation;
            this.cannonRotation = global_cannonRotation;
            this.cannonBulletDirRotation = cannonBulletRotation;

            //rotate the lookforward vector (vector that is applied by tankRotation)
            lookforward = Vector3.Transform(-Vector3.UnitZ, tankRotation * leftRightSteerRotation);
            lookforward = Vector3.Normalize(lookforward);

            //rotate the up vector (vector that is applied by tankRotation)
            //up = Vector3.Transform(Vector3.Up, tankRotation);
            //up = Vector3.Normalize(up);

            //rotate the cannon vector (vector that is applied by global_cannonRotation)
            cannonForward = Vector3.Transform(-Vector3.UnitZ, global_cannonRotation);
            cannonForward = Vector3.Normalize(cannonForward);

            //rotate the cannon bullet direction vector (vector that is applied by cannonBulletRotation)
            cannonBulletDir = Vector3.Transform(-Vector3.UnitZ, cannonBulletRotation);
            cannonBulletDir = Vector3.Normalize(cannonBulletDir);

            tank_right_back_wheel_bone.Transform = leftRightBackWheelRotation * tank_right_back_wheel_transform;
            tank_left_back_wheel_bone.Transform = leftRightBackWheelRotation * tank_left_back_wheel_transform;
            tank_right_steer_bone.Transform = leftRightSteerRotation * tank_right_steer_transform;
            tank_left_steer_bone.Transform = leftRightSteerRotation * tank_left_steer_transform;
            tank_left_front_wheel_bone.Transform = leftRightFrontWheelRotation * tank_left_front_wheel_transform;
            tank_right_front_wheel_bone.Transform = leftRightFrontWheelRotation * tank_right_front_wheel_transform;
            tank_hatch_bone.Transform = hatchRotation * tank_hatch_transform;
            tank_cannon_bone.Transform = cannonRotation * tank_cannon_transform;
            tank_turret_bone.Transform = turretRotation * tank_turret_transform;

            // (use the order-of-important technique to avoid using quaternion)
            tank_bone.Transform = upDownTankRotation * leftRightTankRotation * tank_transform;
            tank_bone.Transform *= leftRightTankRotationY_RealisticRotation;
            tank_bone.Transform *= this.world;
        }

        //Draw (without light)
        public void Draw_noLight(GameTime gameTime, Matrix view, Matrix projection)
        {
            tank.CopyAbsoluteBoneTransformsTo(tank_transforms);

            foreach (ModelMesh mesh in tank.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = tank_transforms[mesh.ParentBone.Index];
                    effect.LightingEnabled = false;
                }
                mesh.Draw();
            }
        }

        //Draw (with default light)
        public void Draw_defaultLight(GameTime gameTime, Matrix view, Matrix projection)
        {
            tank.CopyAbsoluteBoneTransformsTo(tank_transforms);

            foreach (ModelMesh mesh in tank.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = tank_transforms[mesh.ParentBone.Index];
                    effect.EnableDefaultLighting();
                }
                mesh.Draw();
            }
        }

        //Draw (with light adjust)
        public void Draw_basicEffect(GameTime gameTime, Matrix view, Matrix projection)
        {
            tank.CopyAbsoluteBoneTransformsTo(tank_transforms);

            int i = 0;

            foreach (ModelMesh mesh in tank.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = tank_transforms[mesh.ParentBone.Index];
                    effect.AmbientLightColor = LightSettings.AmbientLightColor * LightSettings.LightPower * LightSettings.ModelLightFactor;
                    effect.DirectionalLight0.DiffuseColor = Color.White.ToVector3();
                    effect.DirectionalLight0.Direction = LightSettings.LightLookAt - LightSettings.LightPosition;
                    effect.DirectionalLight0.Enabled = LightSettings.LightEnabled;
                    effect.TextureEnabled = true;
                    effect.Texture = textures[i++];

                    //specular lighting for light source
                    if(LightSettings.LightSpecularColorCustomEnabled)
                    {
                        effect.DirectionalLight0.SpecularColor = LightSettings.LightSpecularColor * LightSettings.LightSpecularPower;
                    }

                    //specular lighting for object
                    if(LightSettings.SpecularColorCustomEnabled)
                    {
                        effect.SpecularColor = LightSettings.SpecularColor;
                        effect.SpecularPower = LightSettings.SpecularPower;
                    }

                    effect.PreferPerPixelLighting = LightSettings.PerPixelLightingEnabled;
                }
                mesh.Draw();
            }
        }

        //Draw (with light)
        //The world matrix already updated in the Update() method.
        public void Draw(string technique, GameTime gameTime, Matrix view, Matrix projection)
        {
            tank.CopyAbsoluteBoneTransformsTo(tank_transforms);

            int i = 0;

            foreach (ModelMesh mesh in tank.Meshes)
            {
                foreach (Effect effect in mesh.Effects)
                {
                    effect.CurrentTechnique = effect.Techniques[technique];
                    effect.Parameters["xView"].SetValue(view);
                    effect.Parameters["xProjection"].SetValue(projection);
                    effect.Parameters["xLightView"].SetValue(LightSettings.LightView);
                    effect.Parameters["xLightProjection"].SetValue(LightSettings.LightProjection);
                    effect.Parameters["xWorld"].SetValue(tank_transforms[mesh.ParentBone.Index]);
                    effect.Parameters["xAmbient"].SetValue(LightSettings.LightAmbient);
                    effect.Parameters["xLightEnabled"].SetValue(LightSettings.LightEnabled);
                    effect.Parameters["xLightPower"].SetValue(LightSettings.LightPower);
                    effect.Parameters["xLightPosition"].SetValue(LightSettings.LightPosition);
                    effect.Parameters["xLightDirection"].SetValue(LightSettings.LightLookAt - LightSettings.LightPosition);
                    effect.Parameters["xModelLightLevelPower"].SetValue(EffectSettings.eModel.ModelLightFactor);
                    effect.Parameters["xSolidBrown"].SetValue(false);
                    effect.Parameters["xTexture"].SetValue(textures[i++]);
                    effect.Parameters["xLightType"].SetValue(LightSettings.LightType);
                    effect.Parameters["xLightAmbientColor"].SetValue(LightSettings.AmbientLightColor);
                    effect.Parameters["xLightAmbientColorFactor"].SetValue(LightSettings.AmbientLightColorFactor);

                    if (EffectSettings.eShadowedScene.Enabled)
                    {
                        effect.Parameters["xBiasedDistance"].SetValue(EffectSettings.eShadowedScene.BiasedDistance);
                        effect.Parameters["xShadowMap"].SetValue(EffectSettings.eShadowedScene.ShadowMap);
                    }
                    if (EffectSettings.eShadowedScene.BaseCustomDiffuseColorEnabled)
                    {
                        effect.Parameters["xEnabledShadowedSceneCustomInitDiffuseColorFactor"].SetValue(true);
                        effect.Parameters["xBaseDiffuseColorFactor"].SetValue(EffectSettings.eShadowedScene.BaseDiffuseColorFactor);
                    }
                }
                mesh.Draw();
            }
        }
    }
}
