﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TankMovementAnimation
{
    class MouseManager
    {
        float amountX;
        float amountY;
        int halfWidth;
        int halfHeight;
        float scrollDiffAmount;

        const float cameraRotSpeed = 0.3f;

        MouseState originalState;   //original state is the state as which the mouse is at the center
        MouseState preState;
        MouseState currState;

        bool isMouseRelativeEnabled;
        bool isMiddleMouseClicked;
        bool isAlwaysKeepMouseCenter;

        // Get the scroll diff amount
        public float ScrollDiffAmount
        {
            get { return scrollDiffAmount; }
        }

        // Relative mouse mode
        public bool MouseRelativeEnabled
        {
            get { return isMouseRelativeEnabled; }
            set { isMouseRelativeEnabled = value; }
        }

        // Keep mouse at the center as always or not
        public bool AlwaysKeepMouseAtCenter
        {
            get { return isAlwaysKeepMouseCenter; }
            set { isAlwaysKeepMouseCenter = value; }
        }
        
        // Camera rotation
        public float CameraRotX
        {
            get { return amountX; }
        }

        // Camera rotation
        public float CameraRotY
        {
            get { return amountY; }
        }

        // World rotation (inverse of camera)
        public float WorldRotX
        {
            get { return -amountX; }
        }

        // World rotation (inverse of camera)
        public float WorldRotY
        {
            get { return -amountY; }
        }

        //Constructor
        public MouseManager(int width, int height)
        {
            Init(width, height);
        }

        // Set the initial state of mouse
        private void Init(int width, int height)
        {
            halfWidth = width / 2;
            halfHeight = height / 2;
            Mouse.SetPosition(halfWidth, halfHeight);
            originalState = Mouse.GetState();
            preState = Mouse.GetState();
        }

        private void PreUpdate()
        {
            currState = Mouse.GetState();

            //Clear everything first
            isMiddleMouseClicked = false;
        }

        private void PostUpdate()
        {
            //update the previous state
            preState = currState;
        }

        // Normal Update
        public void Update(float diffTime)
        {
            PreUpdate();

            if (isMouseRelativeEnabled)
                UpdateMouseRelative(diffTime, currState);
            else if (isAlwaysKeepMouseCenter)
                PositionMouseAtCenter();
            UpdateScrollDiffAmount(diffTime, currState);
            //All mouse input will be processed here
            ProcessMouseInput(diffTime, currState);

            PostUpdate();
        }

        // Update the scroll diff amount
        private void UpdateScrollDiffAmount(float diffTime, MouseState currState)
        {

            scrollDiffAmount = currState.ScrollWheelValue - preState.ScrollWheelValue;
        }

        // Update the relative mouse
        private void UpdateMouseRelative(float diffTime, MouseState currState)
        {
            if (originalState != currState)
            {
                int relX = currState.X - originalState.X;
                int relY = currState.Y - originalState.Y;

                //use accumulative minus to ease the furthur use
                amountX -= relX * diffTime * cameraRotSpeed;
                amountY -= relY * diffTime * cameraRotSpeed;

                Mouse.SetPosition(originalState.X, originalState.Y);
            }
        }

        // Isolate method to keep mouse at the center as always
        private void PositionMouseAtCenter()
        {
            Mouse.SetPosition(originalState.X, originalState.Y);
        }

        //Process mouse input
        private void ProcessMouseInput(float diffTime, MouseState currState)
        {
            //Middle mouse click
            if (preState.MiddleButton == ButtonState.Released && currState.MiddleButton == ButtonState.Pressed)
            {
                isMiddleMouseClicked = true;
            }
        }

        //Test whether the middle mouse button is clicked
        public bool MiddleMouseClicked()
        {
            return isMiddleMouseClicked;
        }

        // Clear all the amount value
        public void Clear()
        {
            amountX = 0.0f;
            amountY = 0.0f;
        }
    }
}
